#ifndef WEBSOCKETSERVERMANAGER_H
#define WEBSOCKETSERVERMANAGER_H

/************************************************************\
 * 控件名称： WebSocket服务器管理类
 * 控件描述：
 *          1.类似于QTcpSocket操作
 * 作者：红模仿    联系方式：QQ21497936
 * 博客地址：https://blog.csdn.net/qq21497936
 *       日期             版本         描述
 *   2019年09月04日      v1.0.0      基础功能
\************************************************************/

#include <QObject>
#include <QWebSocketServer>
#include <QThread>

class WebSocketServerManager : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketServerManager(QString serverName,
                                    QWebSocketServer::SslMode secureMode = QWebSocketServer::NonSecureMode,
                                    QObject *parent = 0);
    ~WebSocketServerManager();

public:
    bool running() const;

signals:
    void signal_conncted(QString ip, qint32 port);
    void signal_disconncted(QString ip, qint32 port);
    void signal_sendTextMessageResult(QString ip, quint32 port, bool result);
    void signal_sendBinaryMessageResult(QString ip, quint32 port, bool result);
    void signal_error(QString ip, quint32 port, QString errorString);
    void signal_textFrameReceived(QString ip, quint32 port, QString frame, bool isLastFrame);
    void signal_textMessageReceived(QString ip, quint32 port,QString message);
    void signal_close();

public slots:
    void slot_start(QHostAddress hostAddress = QHostAddress(QHostAddress::Any), qint32 port = 10080);
    void slot_stop();
    void slot_sendData(QString ip, qint32 port, QString message);

protected slots:
    void slot_newConnection();
    void slot_serverError(QWebSocketProtocol::CloseCode closeCode);
    void slot_closed();

protected slots:
    void slot_disconnected();
    void slot_error(QAbstractSocket::SocketError error);
    void slot_textFrameReceived(const QString &frame, bool isLastFrame);
    void slot_textMessageReceived(const QString &message);

private:
    QString _serverName;
    QWebSocketServer::SslMode _sslMode;
    bool _running;
    QWebSocketServer *_pWebSocketServer;
    QHash<QString, QWebSocket*> _hashIpPort2PWebSocket;
    QHostAddress _listenHostAddress;
    qint32 _listenPort;
};


#endif // WEBSOCKETSERVERMANAGER_H
