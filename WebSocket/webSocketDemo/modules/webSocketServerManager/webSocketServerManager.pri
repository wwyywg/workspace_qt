INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT += websockets

HEADERS += \
    $$PWD/WebSocketServerManager.h

SOURCES += \
    $$PWD/WebSocketServerManager.cpp
