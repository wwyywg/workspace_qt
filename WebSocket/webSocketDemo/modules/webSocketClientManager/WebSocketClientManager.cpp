#include "WebSocketClientManager.h"
#include <QDebug>

WebSocketClientManager::WebSocketClientManager(QObject *parent)
    : QObject(parent),
    _running(false),
    _pWebSocket(0),
    _connected(false)
{

}

WebSocketClientManager::~WebSocketClientManager()
{
    if(_pWebSocket != 0)
    {
        _pWebSocket->deleteLater();
        _pWebSocket = 0;
    }
}

bool WebSocketClientManager::running() const
{
    return _running;
}

void WebSocketClientManager::slot_start()
{
    if(_running)
    {
        qDebug() << __FILE__ << __LINE__
                 << "Failed to" << __FUNCTION__ << "it's already running...";
        return;
    }
    if(!_pWebSocket)
    {
        _pWebSocket = new QWebSocket();
        connect(_pWebSocket, SIGNAL(connected())   , this, SLOT(slot_connected())   );
        connect(_pWebSocket, SIGNAL(disconnected()), this, SLOT(slot_disconnected()));
        connect(_pWebSocket, SIGNAL(error(QAbstractSocket::SocketError)),
                this       , SLOT(slot_error(QAbstractSocket::SocketError)));
        connect(_pWebSocket, SIGNAL(textFrameReceived(QString,bool)),
                this       , SLOT(slot_textFrameReceived(QString,bool)));
        connect(_pWebSocket, SIGNAL(textMessageReceived(QString)),
                this       , SLOT(slot_textMessageReceived(QString)));
    }
    _running = true;
}

void WebSocketClientManager::slot_stop()
{
    if(!_running)
    {
        qDebug() << __FILE__ << __LINE__
                 << "Failed to" << __FUNCTION__
                 << ", it's not running...";
        return;
    }
    _running = false;
    _pWebSocket->close();
}

void WebSocketClientManager::slot_connectedTo(QString url)
{
    if(!_running)
    {
        qDebug() << __FILE__ << __LINE__
                 << "Failed to" << __FUNCTION__
                 << ", it's not running...";
        return;
    }
    _pWebSocket->open(QUrl(url));
}

void WebSocketClientManager::slot_sendTextMessage(const QString &message)
{
    if(!_running)
    {
        qDebug() << __FILE__ << __LINE__
                 << "Failed to" << __FUNCTION__
                 << ", it's not running...";
        return;
    }
    bool result = true;
    _pWebSocket->sendTextMessage(message);
    emit signal_sendTextMessageResult(result);
}

void WebSocketClientManager::slot_sendBinaryMessage(const QByteArray &data)
{
    if(!_running)
    {
        qDebug() << __FILE__ << __LINE__
                 << "Failed to" << __FUNCTION__
                 << ", it's not running...";
        return;
    }
    bool result = true;
    _pWebSocket->sendBinaryMessage(data);
    emit signal_sendBinaryMessageResult(result);
}

void WebSocketClientManager::slot_connected()
{
    _connected = true;
    qDebug() << __FILE__ << __LINE__ << "connected";
    emit signal_connected();
}

void WebSocketClientManager::slot_disconnected()
{
    _connected = false;
    qDebug() << __FILE__ << __LINE__ << "disconnected";
    emit signal_disconnected();
}

void WebSocketClientManager::slot_error(QAbstractSocket::SocketError error)
{
    qDebug() << __FILE__ << __LINE__ << (int)error << _pWebSocket->errorString();
    emit signal_error(_pWebSocket->errorString());
}

void WebSocketClientManager::slot_textFrameReceived(const QString &frame, bool isLastFrame)
{
    emit signal_textFrameReceived(frame, isLastFrame);
}

void WebSocketClientManager::slot_textMessageReceived(const QString &message)
{
    emit signal_textMessageReceived(message);
}

QString WebSocketClientManager::url() const
{
    return _url;
}

void WebSocketClientManager::setUrl(const QString &url)
{
    _url = url;
}

