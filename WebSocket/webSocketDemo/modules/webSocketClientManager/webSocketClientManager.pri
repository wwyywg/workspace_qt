INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT += websockets

HEADERS += \
    $$PWD/WebSocketClientManager.h

SOURCES += \
    $$PWD/WebSocketClientManager.cpp
