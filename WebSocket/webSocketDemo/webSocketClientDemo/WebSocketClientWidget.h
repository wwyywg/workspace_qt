#ifndef WEBSOCKETCLIENTWIDGET_H
#define WEBSOCKETCLIENTWIDGET_H

#include <QWidget>
#include "WebSocketClientManager.h"

namespace Ui {
class WebSocketClientWidget;
}

class WebSocketClientWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WebSocketClientWidget(QWidget *parent = 0);
    ~WebSocketClientWidget();

protected slots:
    void slot_connected();
    void slot_disconnected();
    void slot_error(QString errorString);
    void slot_textFrameReceived(const QString &frame, bool isLastFrame);
    void slot_textMessageReceived(const QString &message);

private slots:
    void on_pushButton_connect_clicked();
    void on_pushButton_send_clicked();

    void on_pushButton_disconnect_clicked();

private:
    Ui::WebSocketClientWidget *ui;

    WebSocketClientManager *_pWebSocketClientManager;
};

#endif // WEBSOCKETCLIENTWIDGET_H
