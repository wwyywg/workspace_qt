#include "WebSocketClientWidget.h"
#include "ui_WebSocketClientWidget.h"

WebSocketClientWidget::WebSocketClientWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WebSocketClientWidget),
    _pWebSocketClientManager(0)
{
    ui->setupUi(this);
    setWindowTitle("WebSocket客户端Demo v1.0.0 (作者:红模仿 QQ21497936 博客地址: blog.csdn.net/qq21497936)");

    ui->pushButton_connect->setEnabled(true);
    ui->pushButton_disconnect->setEnabled(false);

    _pWebSocketClientManager = new WebSocketClientManager;
    connect(_pWebSocketClientManager, SIGNAL(signal_connected()),
            this                    , SLOT(slot_connected())    );
    connect(_pWebSocketClientManager, SIGNAL(signal_disconnected()),
            this                    , SLOT(slot_disconnected())    );
    connect(_pWebSocketClientManager, SIGNAL(signal_error(QString)),
            this                    , SLOT(slot_error(QString))    );
//    connect(_pWebSocketClientManager, SIGNAL(signal_textFrameReceived(QString,bool)),
//            this                    , SLOT(slot_textFrameReceived(QString,bool)) );
    connect(_pWebSocketClientManager, SIGNAL(signal_textMessageReceived(QString)),
            this                    , SLOT(slot_textMessageReceived(QString))    );

}

WebSocketClientWidget::~WebSocketClientWidget()
{
    delete ui;
}

void WebSocketClientWidget::slot_connected()
{
    ui->pushButton_connect->setEnabled(false);
    ui->pushButton_disconnect->setEnabled(true);
    ui->pushButton_send->setEnabled(true);
}

void WebSocketClientWidget::slot_disconnected()
{
    ui->pushButton_connect->setEnabled(true);
    ui->pushButton_disconnect->setEnabled(false);
    ui->pushButton_send->setEnabled(false);
}

void WebSocketClientWidget::slot_error(QString errorString)
{

}

void WebSocketClientWidget::slot_textFrameReceived(const QString &frame, bool isLastFrame)
{
    ui->textEdit_recvData->setText((ui->textEdit_recvData->toPlainText().size() == 0 ? "" : "\n")
                                   + ui->textEdit_recvData->toPlainText()
                                   + QDateTime::currentDateTime().toString("yyyy-HH-mm hh:MM:ss:zzz")
                                   + "\n"
                                   + frame);
}

void WebSocketClientWidget::slot_textMessageReceived(const QString &message)
{
    ui->textEdit_recvData->setText((ui->textEdit_recvData->toPlainText().size() == 0 ? "" : "\n")
                                   + ui->textEdit_recvData->toPlainText()
                                   + QDateTime::currentDateTime().toString("yyyy-HH-mm hh:MM:ss:zzz")
                                   + "\n"
                                   + message);
}

void WebSocketClientWidget::on_pushButton_connect_clicked()
{
    _pWebSocketClientManager->slot_start();
    _pWebSocketClientManager->slot_connectedTo(ui->lineEdit_url->text());
}

void WebSocketClientWidget::on_pushButton_send_clicked()
{
    _pWebSocketClientManager->slot_sendTextMessage(ui->textEdit_sendData->toPlainText());
}

void WebSocketClientWidget::on_pushButton_disconnect_clicked()
{
    _pWebSocketClientManager->slot_stop();
}
