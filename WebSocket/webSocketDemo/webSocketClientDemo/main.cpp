#include "WebSocketClientWidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WebSocketClientWidget w;
    w.show();

    return a.exec();
}
