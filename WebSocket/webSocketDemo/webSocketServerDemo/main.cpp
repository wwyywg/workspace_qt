#include "webSocketServerWidget.h"
#include <QApplication>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    webSocketServerWidget w;
    w.show();

    return a.exec();
}
