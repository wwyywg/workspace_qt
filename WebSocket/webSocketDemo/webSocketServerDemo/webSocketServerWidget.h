#ifndef WEBSOCKETSERVERWIDGET_H
#define WEBSOCKETSERVERWIDGET_H

#include <QWidget>
#include "WebSocketServerManager.h"
#include <QStringListModel>

namespace Ui {
class webSocketServerWidget;
}

class webSocketServerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit webSocketServerWidget(QWidget *parent = 0);
    ~webSocketServerWidget();


protected slots:
    void slot_conncted(QString ip, qint32 port);
    void slot_disconncted(QString ip, qint32 port);
    void slot_sendTextMessageResult(QString ip, quint32 port, bool result);
    void slot_sendBinaryMessageResult(QString ip, quint32 port, bool result);
    void slot_error(QString ip, quint32 port, QString errorString);
    void slot_textFrameReceived(QString ip, quint32 port, QString frame, bool isLastFrame);
    void slot_textMessageReceived(QString ip, quint32 port,QString message);
    void slot_close();

protected:
    void updateTextEdit();
    QString getLocalIp();

private slots:
    void on_pushButton_listen_clicked();
    void on_pushButton_stop_clicked();
    void on_listView_ipPort_clicked(const QModelIndex &index);


    void on_pushButton_send_clicked();

private:
    Ui::webSocketServerWidget *ui;
    WebSocketServerManager *_pWebSocketServerManager;
    QStringList _strList;
    QStringListModel _model;
    QHash<QString, QString> _hashIpPort2Message;
};

#endif // WEBSOCKETSERVERWIDGET_H
