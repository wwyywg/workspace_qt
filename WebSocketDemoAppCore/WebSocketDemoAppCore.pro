QT += websockets network

SOURCES += \
    WebSocketServer.cpp \
    WebSocketServerManager.cpp \
    main.cpp

HEADERS += \
    WebSocketServer.h \
    WebSocketServerManager.h
