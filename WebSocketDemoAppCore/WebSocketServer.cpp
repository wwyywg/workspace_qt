#include "WebSocketServer.h"
#include <QDateTime>
#include <QNetworkInterface>

WebSocketServer::WebSocketServer(QObject *parent) : QObject(parent)
{
    _pWebSocketServerManager = new WebSocketServerManager("Server");
    connect(_pWebSocketServerManager, SIGNAL(signal_conncted(QString,qint32)),
            this                    , SLOT(slot_conncted(QString,qint32)));
    connect(_pWebSocketServerManager, SIGNAL(signal_disconncted(QString,qint32)),
            this                    , SLOT(slot_disconncted(QString,qint32)));
    connect(_pWebSocketServerManager, SIGNAL(signal_error(QString,quint32,QString)),
            this                    , SLOT(slot_error(QString,quint32,QString)));
    connect(_pWebSocketServerManager, SIGNAL(signal_textFrameReceived(QString,quint32,QString,bool)),
            this                    , SLOT(slot_textFrameReceived(QString,quint32,QString,bool)));
    connect(_pWebSocketServerManager, SIGNAL(signal_textMessageReceived(QString,quint32,QString)),
            this                    , SLOT(slot_textMessageReceived(QString,quint32,QString)));
}

WebSocketServer::~WebSocketServer()
{

}

void WebSocketServer::slot_conncted(QString ip, qint32 port)
{
    _strList << QString("%1-%2").arg(ip).arg(port);
    _hashIpPort2Message.insert(QString("%1-%2").arg(ip).arg(port), QString());
}

void WebSocketServer::slot_disconncted(QString ip, qint32 port)
{
    QString str = QString("%1-%2").arg(ip).arg(port);
    if(_strList.contains(str))
    {
        _strList.removeOne(str);
    }
}

void WebSocketServer::slot_sendTextMessageResult(QString ip, quint32 port, bool result)
{

}

void WebSocketServer::slot_sendBinaryMessageResult(QString ip, quint32 port, bool result)
{

}

void WebSocketServer::slot_error(QString ip, quint32 port, QString errorString)
{

}

void WebSocketServer::slot_textFrameReceived(QString ip, quint32 port, QString frame, bool isLastFrame)
{
    if(_hashIpPort2Message.contains(QString("%1-%2").arg(ip).arg(port)))
    {
        updateSend(ip, port, frame);
    }
}

/**
 * @brief 收到某个客户端的数据
 * @param ip
 * @param port
 * @param message
 */
void WebSocketServer::slot_textMessageReceived(QString ip, quint32 port, QString message)
{
    qDebug() << __FILE__ << __LINE__ << message;
    if(_hashIpPort2Message.contains(QString("%1-%2").arg(ip).arg(port)))
    {
        updateSend(ip, port, message);
    }
}

/**
 * @brief 关闭服务器 清空数据
 */
void WebSocketServer::slot_close()
{
    _pWebSocketServerManager->slot_stop();
    _strList.clear();
}

/**
 * @brief 获取本地IP
 * @return
 */
QString WebSocketServer::getLocalIp()
{
    QString localIp;
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    for(int index = 0; index < list.size(); index++)
    {
        if(list.at(index).protocol() == QAbstractSocket::IPv4Protocol)
        {
            if(list.at(index).toString().contains("127.0"))
            {
                continue;
            }
            localIp = list.at(index).toString();
            break;
        }
    }
    return localIp;
}

/**
 * @brief 监听服务的端口
 * @param port
 */
void WebSocketServer::listen(qint32 port)
{
    _pWebSocketServerManager->slot_start(QHostAddress::Any, port);
}

void WebSocketServer::slot_stop()
{
    _pWebSocketServerManager->slot_stop();
}

/**
 * @brief 连接的客户端发送数据
 * @param ip
 * @param port
 * @param message
 */
void WebSocketServer::updateSend(QString ip, qint32 port, QString message)
{
    QHash<QString, QString>::const_iterator iter;
    for(iter = _hashIpPort2Message.constBegin(); iter != _hashIpPort2Message.constEnd(); ++iter)
    {
        qDebug() << iter.key();
        QStringList strList = iter.key().split("-");
        if(strList.size() == 2)
        {
            if(strList.at(0) == ip && strList.at(1).toInt() == port)
            {
                continue;
            }
            else
            {
                _pWebSocketServerManager->slot_sendData(strList.at(0),
                                                        strList.at(1).toInt(),
                                                        message);
            }
        }
    }

}




