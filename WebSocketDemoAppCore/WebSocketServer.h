#ifndef WEBSOCKETSERVER_H
#define WEBSOCKETSERVER_H

#include "WebSocketServerManager.h"
#include <QObject>

class WebSocketServer : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketServer(QObject *parent = nullptr);
    ~WebSocketServer();

signals:

protected slots:
    void slot_conncted(QString ip, qint32 port);
    void slot_disconncted(QString ip, qint32 port);
    void slot_sendTextMessageResult(QString ip, quint32 port, bool result);
    void slot_sendBinaryMessageResult(QString ip, quint32 port, bool result);
    void slot_error(QString ip, quint32 port, QString errorString);
    void slot_textFrameReceived(QString ip, quint32 port, QString frame, bool isLastFrame);
    void slot_textMessageReceived(QString ip, quint32 port,QString message);
    void slot_close();

public:
    void listen(qint32 port);

protected:
    QString getLocalIp();
    void updateSend(QString ip, qint32 port, QString message);

 public slots:
    void slot_stop();

private:
    WebSocketServerManager *_pWebSocketServerManager;
    QStringList _strList;
    QHash<QString, QString> _hashIpPort2Message;

};

#endif // WEBSOCKETSERVER_H
