#include "WebSocketServer.h"
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    WebSocketServer server;
    server.listen(2222);

    return a.exec();
}
