#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTextBrowser>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>
#include <QTextEdit>

class TcpServer : public QWidget
{
    Q_OBJECT

public:
    TcpServer(QWidget *parent = nullptr);
    ~TcpServer();

private:
    QTcpServer* _tcpServer;         // 服务器
    QTcpSocket* _currentClient;     // 当前连接
    QList<QTcpSocket*> _tcpClient;  // 客户端集合

    QPushButton* _btnConnect;   // 连接 断开
    QPushButton* _btnSend;  // 发送
    QPushButton* _btnClear; // 清楚

    QTextBrowser* _show;    // 显示数据
    QTextEdit * _sendTxt;   // 发送框
    QLineEdit* _ipTxt;      // IP主机
    QLineEdit* _portTxt;    // 端口号

    QComboBox* _cbxConnection;

signals:

public slots:
    void NewConnectionSlot();
    void disConnectedSlot();
    void ReadData();

    void on_btnConnect_clicked();
    void on_btnSend_clicked();
    void on_btnClear_clicked();
};
#endif // TCPSERVER_H
