#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QTcpSocket>
#include <QHostAddress>
#include <QMessageBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QPushButton>
#include <QTextEdit>

class TcpClient : public QWidget
{
    Q_OBJECT
public:
    explicit TcpClient(QWidget *parent = nullptr);

private:
    QTcpSocket* _tcpClient; // 客户端

    QPushButton* _btnConnect;   // 连接 断开
    QPushButton* _btnSend;  // 发送

    QTextBrowser* _show;    // 消息框
    QTextEdit* _sendTxt;    // 发送框
    QLineEdit* _ipTxt;      // IP主机
    QLineEdit* _portTxt;    // 端口号

signals:

private slots:
    void ReadData();
    void ReadError(QAbstractSocket::SocketError);

    void on_btnConnect_clicked();   // 连接服务器
    void on_btnSend_clicked();     // 发送数据
    void on_pushButton_clicked();

};

#endif // TCPCLIENT_H
