#include "TcpServer.h"
#include "TcpClient.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TcpServer s;
    s.show();
    s.setWindowTitle("服务器");
    s.resize(600, 600);
    s.setGeometry(200, 100, s.width(), s.height());

    TcpClient c;
    c.show();
    c.setWindowTitle("客户端");
    c.resize(600, 600);
    c.setGeometry(800, 100, c.width(), c.height());

    return a.exec();
}
