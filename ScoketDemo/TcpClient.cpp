#include "TcpClient.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>

TcpClient::TcpClient(QWidget *parent) : QWidget(parent)
{
    // 初始化TCP客户端
    _tcpClient = new QTcpSocket(this);  // 实例化 _tcpClient
    _tcpClient->abort();                // 取消原有连接

    _btnConnect = new QPushButton("连接");
    _btnConnect->setEnabled(true);
    _btnSend = new QPushButton("发送");
    _btnSend->setFixedHeight(60);
    _btnSend->setEnabled(false);

    QVBoxLayout* vMainLayout = new QVBoxLayout;
    QHBoxLayout* hLayout = new QHBoxLayout;     // 水平布局

    QVBoxLayout* vLayout1 = new QVBoxLayout;
    _show = new QTextBrowser;   // 显示
    _show->setFixedWidth(400);
    _show->setFixedHeight(400);
    vLayout1->addWidget(_show);
    vLayout1->addStrut(1);

    QVBoxLayout* vLayout = new QVBoxLayout;     // 垂直布局
    QLabel* lblIP = new QLabel("服务器IP地址");
    vLayout->addWidget(lblIP);
    _ipTxt = new QLineEdit;
    vLayout->addWidget(_ipTxt);

    QLabel* lblPort = new QLabel("服务器端口号");
    vLayout->addWidget(lblPort);

    _portTxt = new QLineEdit;           // 端口控件实例化
    vLayout->addWidget(_portTxt);
    vLayout->addWidget(_btnConnect);

    vLayout->addStretch(1);

    hLayout->addLayout(vLayout1);
    hLayout->addLayout(vLayout);
    hLayout->addStrut(1);

    QHBoxLayout* hLayout1 = new QHBoxLayout;
    _sendTxt = new QTextEdit;   // 发送框
    _sendTxt->setFixedHeight(60);
    hLayout1->addWidget(_sendTxt);
    hLayout1->addWidget(_btnSend);

    vMainLayout->addLayout(hLayout);
    vMainLayout->addLayout(hLayout1);

    this->setLayout(vMainLayout);

    connect(_btnConnect, SIGNAL(clicked()), this, SLOT(on_btnConnect_clicked()));
    connect(_btnSend, SIGNAL(clicked()), this, SLOT(on_btnSend_clicked()));
//    connect(_btnClear, SIGNAL(clicked()), this, SLOT(on_btnClear_clicked()));
    connect(_tcpClient, SIGNAL(readyRead()), this, SLOT(ReadData()));
    connect(_tcpClient, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ReadError(QAbstractSocket::SocketError)));
}

/**
 * 读取服务器发送过来的数据
 * @brief TcpClient::ReadData
 */
void TcpClient::ReadData()
{
    QByteArray buffer = _tcpClient->readAll();
    if(!buffer.isEmpty())
    {
        _show->append(buffer);
    }
}

/**
 *
 * @brief TcpClient::ReadError
 */
void TcpClient::ReadError(QAbstractSocket::SocketError)
{
    _tcpClient->disconnectFromHost();   // 断开连接
    _btnConnect->setText(tr("连接"));
    QMessageBox msgBox;
    msgBox.setText(tr("failed to connect server because %1").arg(_tcpClient->errorString()));
}

/**
 * 连接服务器
 * @brief TcpClient::on_btnConnect_clicked
 */
void TcpClient::on_btnConnect_clicked()
{
    qDebug() << "连接服务器";
    // 连接服务器
    _tcpClient->connectToHost(_ipTxt->text(), _portTxt->text().toUInt());
    if(_tcpClient->waitForConnected(1000)) {
        _btnConnect->setText("断开");
        _btnSend->setEnabled(true);
    }

    // 已断开连接
//    _tcpClient->disconnectFromHost();
//    if(_tcpClient->state() == QAbstractSocket::UnconnectedState || _tcpClient->waitForDisconnected(1000))
//    {
//        _btnConnect->setText("连接");
//        _btnSend->setEnabled(false);
//    }
}

void TcpClient::on_btnSend_clicked()
{
    QString data = _sendTxt->toPlainText();
    if(data.isEmpty()) return;

    _tcpClient->write(data.toUtf8());
    _sendTxt->clear();
}

void TcpClient::on_pushButton_clicked()
{
    _show->clear();
}

