#include <QApplication>
#include "dialog.h"
#include<QTextCodec>
#include<QSqlDatabase>
#include<QSqlQuery>
#include<QMessageBox>
#include<QObject>

bool connectiondb()//连接java_os数据库
{
    QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("java.db");
    if(!db.open())
    {
        QMessageBox::critical(0,QObject::tr("Database Error"),"Error");
        return false;
    }
    return true;

}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("GBK"));//中文不会乱码
    QTextCodec::setCodecForTr(QTextCodec::codecForLocale());//本地编码

    Dialog s;
    if(connectiondb()==true)
    {

        s.show();
        return a.exec();
    }
    else return 0;

}
