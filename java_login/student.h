#ifndef STUDENT_H
#define STUDENT_H

#include <QWidget>
#include<QTimer>
#include<QDateTime>
#include<QString>
#include"dialog.h"
#include"teacher.h"
#include<QLineEdit>
#include<QComboBox>
#include<QTextEdit>
#include<QMessageBox>
#include<QDebug>
#include<QSqlRelationalTableModel>
#include<QPalette>
extern int grade_no;
extern QString answer_fill;


namespace Ui {
class Student;

}

class student_password :public QDialog//学生修改密码模块
{
    Q_OBJECT

public:
    student_password(QWidget *parent =0);
    ~student_password();
private slots:
    void corret_password();
    void clear_text();
private:
    QLabel *label_1;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *LineEdit_1;
    QLineEdit *LineEdit_2;
    QLineEdit *LineEdit_3;
    QPushButton *button_1;
    QPushButton *button_2;


};


class student_update_information:public QDialog//学生修改信息模块
{
    Q_OBJECT
public:
    student_update_information(QWidget *parent=0);
    ~student_update_information();

private slots:
    void to_clear_all();
    void to_sumitall();
    void enable_button(const QString &text);

private:
    QLabel *student_id;//不可编辑
    QLabel *student_name;//不可编辑
    QLabel *student_sex;//不可编辑
    QLabel *student_department;//不可编辑
    QLabel *student_tel;
    QLabel *student_class;//不可编辑
    QLabel *student_cno1;
    QLabel *student_cno2;
    QLineEdit *LineEdit_id;
    QLineEdit *LineEdit_name;
    QComboBox *ComboBox_sex;
    QLineEdit *LineEdit_department;
    QLineEdit *LineEdit_tel;
    QLineEdit *LineEdit_class;
    QLineEdit *LineEdit_cno1;
    QLineEdit *LineEdit_cno2;
    QPushButton *button_1;
    QPushButton *button_2;

};

class student_need_help:public QDialog//学生请求帮助模块
{
    Q_OBJECT

public:
    student_need_help(QWidget *parent=0);
    ~student_need_help();
private slots:
    void student_clear_all();
    void student_summitall();

private:
    QLabel *label;
    QTextEdit *textedit;
    QPushButton *button1;
    QPushButton *button2;


};

class Student : public QWidget//---主模块----
{
    Q_OBJECT
    
public:
    explicit Student(QWidget *parent = 0);
    ~Student();

public:
void student_set_label(int n,QString choose_text,QString choose_a,QString choose_b,QString choose_c,QString choose_d,QString answer);
void student_set_label1(QString fill_text);
private slots:
    void student_update_time();
    void student_ol_test();//转到在线考试
    void student_start_test();//学生开始在线考试
    void student_free_talk();//转到学生交流中心
    void student_t_inform();//转到公告中心
    void student_grade();//转到查看成绩
    void student_get_problem();//获取选择槽题函数
    void student_get_fill_problem();//获取填空题槽函数
    void student_put_fill_answer();//填空题确定槽函数
    void student_clear_fill_answer();//填空题清除槽函数
    void student_sum_grade();//提交试卷槽函数--计算分数
    void student_back_time();//考试倒计时实现

    void student_back();//返回登录界面
    void student_close();//关闭界面
    void student_update_password();//修改密码
    void student_information();//个人信息
    void student_help();//请求帮助

    void student_free_talk_corret();//在线交流的槽函数
    void student_free_talk_clear();

private:
    Ui::Student *ui;
    class Dialog *dui;
    QTimer *student_timer;
    QTimer *student_timer1;
    student_password *pass;
    student_update_information *inform;
    student_need_help *help;
    QSqlRelationalTableModel *model;
    int no_repeat[20];
    int no_repeat_fill[10];
    QButtonGroup *groupbutton;
    int grade[30];
    QString course_id;
    int h,m,s,ms;

};

#endif // STUDENT_H
