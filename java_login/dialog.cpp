#include "dialog.h"
#include "ui_dialog.h"
QString sno="hh";
QString tno="ss";


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    setWindowTitle(tr("登录客户端"));//设置窗口标题
    //this->setWindowFlags(Qt::Dialog|Qt::FramelessWindowHint);//去除标题和边框

    Qt::WindowFlags flags=Qt::Dialog;//去除对话框右上角“帮助”按钮
    flags |=Qt::WindowCloseButtonHint;
    setWindowFlags(flags);

    this->setFixedSize( this->width (),this->height ());//固定窗口大小
    ui->lineEdit->setStyleSheet("font: 11pt \"Comic Sans MS\";");
    //ui->lineEdit_2->setStyleSheet("font: 11pt \"Comic Sans MS\";");
    ui->lineEdit_2->setEchoMode(QLineEdit::Password);//设置密码输入时显示****

    timer=new QTimer(this);//添加时间组件
    connect(timer,SIGNAL(timeout()),this,SLOT(update_time()));
    timer->start(1000);

    buttongroup=new QButtonGroup;
    buttongroup->addButton(ui->radioButton,0);
    buttongroup->addButton(ui->radioButton_2,1);
    buttongroup->addButton(ui->radioButton_3,2);

    connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(about_author()));//转到“关于”按钮显示作者信息
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(go_to_student()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(go_to_teacher()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(go_to_administrator()));
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(clear_all()));



}

void Dialog::go_to_student()//转到学生界面
{
    if(buttongroup->checkedId()==0)
    {
        QSqlQuery query;
        query.exec("SELECT sno,spassword FROM student");
        while(query.next())
        {
            QString ssno=query.value(0).toString();
            QString spassword=query.value(1).toString();
            if(ui->lineEdit->text()==ssno&&ui->lineEdit_2->text()==spassword)
            {
                this->hide();

                QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("登录成功！"));
                box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.setButtonText (QMessageBox::Cancel,tr("取 消"));
                box.exec ();

                sno=ssno;
                sui=new Student;//传递学号到学生界面
                sui->show();//跳转到学生界面
                return ;
            }
    }
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("用户名或密码错误!"));//弹出警告框
        box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.setButtonText (QMessageBox::Cancel,tr("取 消"));
        box.exec ();

        ui->lineEdit->clear();//清除用户名的lineEdit
        ui->lineEdit_2->clear();//清除密码的lineEdit
        ui->lineEdit->setFocus();//将光标移到用户名输入
     }

}

void Dialog::go_to_teacher()//转到教师界面
{
    if(buttongroup->checkedId()==1)
    {
        QSqlQuery query;
        query.exec("SELECT tno,tpassword FROM teacher");
         while(query.next())
         {
             QString ttno=query.value(0).toString();
             QString tpassword=query.value(1).toString();
             if(ui->lineEdit->text()==ttno&&ui->lineEdit_2->text()==tpassword)
             {
                 this->hide();
                 tno=ttno;

                 QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("登录成功！"));
                 box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
                 box.setButtonText (QMessageBox::Ok,tr("确 定"));
                 box.setButtonText (QMessageBox::Cancel,tr("取 消"));
                 box.exec ();

                 tui=new Teacher;
                 tui->show();
                 return ;
             }

         }
         QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("用户名或密码错误!"));//弹出警告框
         box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
         box.setButtonText (QMessageBox::Ok,tr("确 定"));
         box.setButtonText (QMessageBox::Cancel,tr("取 消"));
         box.exec ();

         ui->lineEdit->clear();//清除用户名的lineEdit
         ui->lineEdit_2->clear();//清除密码的lineEdit
         ui->lineEdit->setFocus();//将光标移到用户名输入


    }
}

void Dialog::go_to_administrator()//转到管理员界面
{
    if(buttongroup->checkedId()==2)
    {
        QString administrator_id="1";
        QString administrator_password="1";
        if(ui->lineEdit->text()==administrator_id&&ui->lineEdit_2->text()==administrator_password)
        {
            this->hide();

            QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("登录成功！"));
            box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
            box.setButtonText (QMessageBox::Ok,tr("确 定"));
            box.setButtonText (QMessageBox::Cancel,tr("取 消"));
            box.exec ();

            aui=new Administractor;
            aui->show();
            return ;
        }
        else
        {
            QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("用户名或密码错误!"));//弹出警告框
            box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
            box.setButtonText (QMessageBox::Ok,tr("确 定"));
            box.setButtonText (QMessageBox::Cancel,tr("取 消"));
            box.exec ();

            ui->lineEdit->clear();
            ui->lineEdit_2->clear();
            ui->lineEdit->setFocus();

        }
    }
}

void Dialog::clear_all()
{
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit->setFocus();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::about_author()
{
    QMessageBox box(QMessageBox::Question,tr("制作者"),tr("余海帆\n联系方式：18219547577  \n地址：肇庆学院D1--342"));
    box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.setButtonText (QMessageBox::Cancel,tr("取 消"));
    box.exec ();

}

void Dialog::update_time()//时间变化函数
{
    QDateTime time=QDateTime::currentDateTime();
    QString str=time.toString("yyyy-MM-dd hh:mm:ss dddd");
    ui->label_5->setText(str);
}
