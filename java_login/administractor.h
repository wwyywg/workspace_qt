#ifndef ADMINISTRACTOR_H
#define ADMINISTRACTOR_H

#include <QWidget>
#include<QTimer>
#include<QDateTime>
#include<QString>
#include<QSqlRelationalTableModel>


namespace Ui {
class Administractor;
}

class Administractor : public QWidget
{
    Q_OBJECT
    
public:
    explicit Administractor(QWidget *parent = 0);
    ~Administractor();
    
private slots:
    void admini_update_time();
    void student_manage();//学生管理
    void teacher_manage();//教师管理
    void course_manage();//课程管理
    void ad_say();//发布公告
    void question_table();//疑问中心

    void select_record();//查询操作
    void update_record();//修改操作
    void delete_record();//删除操作
    void insert_record();//添加操作
    void submmit_record();//提交操作
    void exit();//注销操作
    void submmit_inform();//提交公告
    void clear_text();//清除公告的输入框

private:
    Ui::Administractor *ui;
    QTimer *admini_timer;
    QSqlRelationalTableModel *model;
};

#endif // ADMINISTRACTOR_H
