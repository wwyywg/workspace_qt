#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include<QDateTime>
#include<QTimer>
#include<QMessageBox>
#include<QButtonGroup>
#include<QSqlQuery>
#include"administractor.h"
#include"student.h"
#include"teacher.h"
#include<QString>
extern QString sno;
extern QString tno;

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
private slots:
    void update_time();
    void about_author();

    void go_to_student();
    void go_to_teacher();
    void go_to_administrator();
    void clear_all();
    
private:
    Ui::Dialog *ui;
    QTimer *timer;
    class Student *sui;
    class Teacher *tui;
    Administractor *aui;
    QButtonGroup *buttongroup;
};

#endif // DIALOG_H
