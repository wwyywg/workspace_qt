#include "student.h"
#include "ui_student.h"
#include<QFont>
#include<QSqlQuery>
int grade_no=0;
QString answer_fill="xx";

Student::Student(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Student)
{
    ui->setupUi(this);
    setWindowTitle(tr("学生客户端"));//设置窗口标题
    this->setFixedSize(this->width(),this->height());//固定窗口大小
    h=2;m=0;s=0;ms=0;
    model=new QSqlRelationalTableModel(this);//初始化model

    int t;//产生不重复的1--20的随机整型数--随机用于抽取选择试题
    QTime time1=QTime::currentTime();
    qsrand(time1.msec()+time1.second()*1000);
    for(int i=0;i<20;)
    {
        t=qrand()%20+1;
        int m=0;
        while(m<i&&no_repeat[m]!=t) m++;
        if(m==i)
        {
           no_repeat[i]=t;
           i++;
         }
     }


    //设置来自教师的消息
    QSqlQuery query1;
    query1.exec("select t_inform_no,tno,neirong from t_inform");
    while(query1.last())
    {
        QString str=query1.value(2).toString();
        QMessageBox box(QMessageBox::Information,tr("教师消息"),str+tr("\n"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec();
        break;
    }
    int t_fill;//产生不重复的1--10的随机整型数--用于随机抽取填空试题
    QTime time2=QTime::currentTime();
    qsrand(time2.msec()+time2.second()*1000);
    for(int j=0;j<10;)
    {
        t_fill=qrand()%10+1;
        int m_fill=0;
        while(m_fill<j&&no_repeat_fill[m_fill]!=t_fill) m_fill++;
        if(m_fill==j)
        {
           no_repeat_fill[j]=t_fill;
           j++;
         }
     }

    int j=0;
    for(;j<10;j++)
        qDebug()<<no_repeat_fill[j];

    for(int k=0;k<30;k++)//给成绩的数组赋初值0
    {
        grade[k]=0;
    }


    groupbutton=new QButtonGroup;//考试ABCD选项
    groupbutton->addButton(ui->radioButton_1_1,0);
    groupbutton->addButton(ui->radioButton_1_2,1);
    groupbutton->addButton(ui->radioButton_1_3,2);
    groupbutton->addButton(ui->radioButton_1_4,3);

    ui->stackedWidget->setCurrentIndex(4);//设置当前显示的页面

    student_timer=new QTimer(this);//设置左下角的时间
    student_timer->start(1000);
    connect(student_timer,SIGNAL(timeout()),this,SLOT(student_update_time()));

    ui->lcdNumber->setDigitCount(8);
    ui->lcdNumber->setMode(QLCDNumber::Dec);
    ui->lcdNumber->setSegmentStyle(QLCDNumber::Flat);
    student_timer1=new QTimer(this);//设置考试倒计时的时间
    student_timer1->start(1000);


    QSqlQuery query;//设置欢迎xxx同学标签
    query.exec("select sno,sname,ssex from student");
    while(query.next())
    {
        QString student_id=query.value(0).toString();
        QString student_name=query.value(1).toString();
        QString student_sex=query.value(2).toString();
        if(student_id==sno)
        {
           ui->label_22->setText(student_name);
           ui->label_22->setStyleSheet("font: 11pt \"Comic Sans MS\";");
           ui->label_23->setText(student_id);
           ui->label_23->setStyleSheet("font: 10pt \"Comic Sans MS\";");
           ui->label_24->setText(student_sex);
           ui->label_24->setStyleSheet("font: 10pt \"Comic Sans MS\";");
           break;

        }
    }

    //由教师选中的选择题
    QSqlQuery query2;
    query2.exec("select * from no_problem");
    while(query2.next())
    {
        QString str_no=query2.value(0).toString();
        QString str_problem_no=query2.value(1).toString();
        if(str_no=="1"&&str_problem_no=="0")
        {
            QSqlQuery query3;
            query3.exec("select * from no_problem");
            query3.next();
            int i;i=0;
            while(query3.next())
            {
                int no=query3.value(1).toInt();
                no_repeat[i]=no;
                i++;
            }
            break;
        }

        else break;


    }
    //由教师选中的填空题
    QSqlQuery query3;
    query3.exec("select * from fill_problem_no");
    while(query3.next())
    {
        QString str_no=query3.value(0).toString();
        QString str_problem_no=query3.value(1).toString();
        if(str_no=="1"&&str_problem_no=="0")
        {
            QSqlQuery query4;
            query4.exec("select * from fill_problem_no");
            query4.next();
            int j;j=0;
            while(query4.next())
            {
                int no=query4.value(1).toInt();
                no_repeat_fill[j]=no;
                j++;
            }
            break;
        }
        else break;
    }
    j=0;
    for(;j<10;j++)
        qDebug()<<no_repeat_fill[j];
    /*设置按钮特效
    QString str_0="QPushButton{border-style: outset;} QPushButton:hover{border-radius: 5px;  border: 5px groove gray;background-color:rgb(0, 255, 255); color: black;} QPushButton:pressed{ border-style: inset; }";
    ui->pushButton_9->setStyleSheet(str_0);//鼠标移动按钮上时按钮变色，按钮凸起，点击凹陷
    ui->pushButton_8->setStyleSheet(str_0);
    ui->pushButton_10->setStyleSheet(str_0);
    //ui->pushButton_11->setStyleSheet(str_0);
    //ui->pushButton_13->setStyleSheet(str_0);*/

    //connect(ui->pushButton_6,SIGNAL(clicked()),this, SLOT(student_save_problem()));//题目收藏
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(student_ol_test()));//在线考试
    connect(ui->pushButton_12,SIGNAL(clicked()),this,SLOT(student_start_test()));//开始在线考试
    //connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(student_free_train()));//自主训练
    connect(ui->pushButton_7,SIGNAL(clicked()),this,SLOT(student_free_talk()));//转到学生交流中心
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(student_t_inform()));//转到公告中心
    connect(ui->pushButton_5,SIGNAL(clicked()),this,SLOT(student_grade()));//转到查看成绩处

    connect(ui->pushButton_13,SIGNAL(clicked()),this,SLOT(student_back()));//返回登录界面
    connect(ui->pushButton_11,SIGNAL(clicked()),this,SLOT(student_close()));//返回主页
    connect(ui->pushButton_9,SIGNAL(clicked()),this,SLOT(student_update_password()));//修改密码
    connect(ui->pushButton_8,SIGNAL(clicked()),this,SLOT(student_information()));//个人信息
    connect(ui->pushButton_10,SIGNAL(clicked()),this,SLOT(student_help()));//请求帮助





}

void Student::student_update_time()//左下角时间变化函数
{
    QDateTime time=QDateTime::currentDateTime();
    QString str=time.toString("yyyy-MM-dd hh:mm:ss dddd");
    ui->label_14->setText(str);

}
void Student::student_back_time()//倒计时变化函数
{
    QTime time = QTime(h,m,s,ms);
    ui->lcdNumber->display(time.toString("hh:mm:ss"));
     s--;
     if(h==0&&m==5&&s==-1)//剩余5分钟时变红色
     {
          QPalette lcdpal = ui->lcdNumber->palette();
          lcdpal.setColor(QPalette::Normal,QPalette::WindowText,Qt::red);
          ui->lcdNumber->setPalette(lcdpal);
     }

      if(s==-1)
      {
          m--;
          s=59;
      }
      if(m==-1)
      {
          h--;
          m=59;
      }
      if(h==0&&m==0&&s==-1)
      {
          Student::student_sum_grade();

      }

}


void Student::student_ol_test()//在线考试
{
    QTime time = QTime(h,m,s,ms);
    QSqlQuery query;
    query.exec("select sno,cno from ol_test");
    while(query.next())
    {
        QString student_id=query.value(0).toString();
        course_id=query.value(1).toString();

        if(student_id==sno)
      {

        QSqlQuery query1;
        query1.exec("select cno,cname from course");
        while(query1.next())
        {
            QString course_id2=query1.value(0).toString();
            QString course_name=query1.value(1).toString();
            if(course_id==course_id2)
            {
                QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("需要考试的科目是：")+course_name);
                box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.setButtonText (QMessageBox::Cancel,tr("取 消"));
                box.exec ();

                 ui->stackedWidget->setCurrentIndex(5);
                 ui->stackedWidget_2->setCurrentIndex(1);
                 ui->tabWidget->setCurrentIndex(0);
                 ui->pushButton_14->setEnabled(false);
                 ui->lcdNumber->display(time.toString("hh:mm:ss"));
                 return ;
            }
        }
      }


    }

    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("暂时没有需要考试的科目"));
    box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.setButtonText (QMessageBox::Cancel,tr("取 消"));
    box.exec ();

}

void Student::student_start_test()//开始在线考试
{

    QMessageBox::StandardButton rb=QMessageBox::warning(NULL,tr("系统消息"),tr("一旦开始考试,中途退出将视为0分\n是否开始考试？"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
    if(rb==QMessageBox::No)
    {
        return ;
    }
    else
    {
        bool value;
        QSqlQuery query;
        value=query.prepare("delete from ol_test where sno=:str_sno and cno=:str_cno");
        if(value)
        {
            query.bindValue(":str_sno",sno);
            query.bindValue(":str_cno",course_id);
            query.exec();
        }
        ui->pushButton_14->setEnabled(true);
        connect(ui->pushButton_14,SIGNAL(clicked()),this,SLOT(student_sum_grade()));
        connect(student_timer1,SIGNAL(timeout()),this,SLOT(student_back_time()));//倒计时开始
        ui->tabWidget->setCurrentIndex(0);
        //去掉关闭按钮
        //setWindowFlags(Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
        ui->pushButton->setEnabled(false);//点击考试考试后程序某些功能将变不可用
        ui->pushButton_2->setEnabled(false);


        ui->pushButton_5->setEnabled(false);

        ui->pushButton_7->setEnabled(false);
        ui->pushButton_10->setEnabled(false);
        ui->pushButton_11->setEnabled(false);
        ui->pushButton_12->setEnabled(false);
        ui->pushButton_13->setEnabled(false);
        //选择题的connect
        connect(ui->pushbutton_1_1,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_2,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_3,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_4,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_5,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_6,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_7,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_8,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_9,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_10,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_11,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_12,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_13,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_14,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_15,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_16,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_17,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_18,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_19,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        connect(ui->pushbutton_1_20,SIGNAL(clicked()),this,SLOT(student_get_problem()));
        ui->pushbutton_1_1->setObjectName("1");
        ui->pushbutton_1_2->setObjectName("2");
        ui->pushbutton_1_3->setObjectName("3");
        ui->pushbutton_1_4->setObjectName("4");
        ui->pushbutton_1_5->setObjectName("5");
        ui->pushbutton_1_6->setObjectName("6");
        ui->pushbutton_1_7->setObjectName("7");
        ui->pushbutton_1_8->setObjectName("8");
        ui->pushbutton_1_9->setObjectName("9");
        ui->pushbutton_1_10->setObjectName("10");
        ui->pushbutton_1_11->setObjectName("11");
        ui->pushbutton_1_12->setObjectName("12");
        ui->pushbutton_1_13->setObjectName("13");
        ui->pushbutton_1_14->setObjectName("14");
        ui->pushbutton_1_15->setObjectName("15");
        ui->pushbutton_1_16->setObjectName("16");
        ui->pushbutton_1_17->setObjectName("17");
        ui->pushbutton_1_18->setObjectName("18");
        ui->pushbutton_1_19->setObjectName("19");
        ui->pushbutton_1_20->setObjectName("20");

        //填空题的connect
        connect(ui->pushbutton_2_1,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_2,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_3,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_4,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_5,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_6,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_7,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_8,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_9,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        connect(ui->pushbutton_2_10,SIGNAL(clicked()),this,SLOT(student_get_fill_problem()));
        ui->pushbutton_2_1->setObjectName("21");
        ui->pushbutton_2_2->setObjectName("22");
        ui->pushbutton_2_3->setObjectName("23");
        ui->pushbutton_2_4->setObjectName("24");
        ui->pushbutton_2_5->setObjectName("25");
        ui->pushbutton_2_6->setObjectName("26");
        ui->pushbutton_2_7->setObjectName("27");
        ui->pushbutton_2_8->setObjectName("28");
        ui->pushbutton_2_9->setObjectName("29");
        ui->pushbutton_2_10->setObjectName("30");

        connect(ui->pushButton_20,SIGNAL(clicked()),this,SLOT(student_put_fill_answer()));
        connect(ui->pushButton_21,SIGNAL(clicked()),this,SLOT(student_clear_fill_answer()));
        emit(ui->pushbutton_1_1->click());


    }


}

//显示选择题函数
void Student::student_set_label(int n, QString choose_text, QString choose_a, QString choose_b, QString choose_c, QString choose_d, QString answer)
{
    ui->label_1_1->setStyleSheet("font: 75 11pt \"Aharoni\";");
    ui->label_1_2->setStyleSheet("font: 75 11pt \"Aharoni\";");
    ui->label_1_3->setStyleSheet("font: 75 11pt \"Aharoni\";");
    ui->label_1_4->setStyleSheet("font: 75 11pt \"Aharoni\";");
    ui->label_1_5->setStyleSheet("font: 75 11pt \"Aharoni\";");
    /*ui->radioButton_1_1->setChecked(false);
    ui->radioButton_1_2->setChecked(false);
    ui->radioButton_1_3->setChecked(false);
    ui->radioButton_1_4->setChecked(false);*/
    ui->label_1_1->setText(choose_text);
    ui->label_1_2->setText(choose_a);
    ui->label_1_3->setText(choose_b);
    ui->label_1_4->setText(choose_c);
    ui->label_1_5->setText(choose_d);
    if(groupbutton->checkedId()==0&&answer=="A")
    {grade[n]=3;}
    else if(groupbutton->checkedId()==1&&answer=="B")
    {grade[n]=3;}
    else if(groupbutton->checkedId()==2&&answer=="C")
    {grade[n]=3;}
    else if(groupbutton->checkedId()==3&&answer=="D")
    {grade[n]=3;}
    else {grade[n]=0;}

}
//显示填空题函数
void Student::student_set_label1(QString fill_text)
{
    ui->textBrowser->setStyleSheet("font: 75 12pt \"Aharoni\";");
    ui->textBrowser->setText(fill_text);

    return ;

}
void Student::student_clear_fill_answer()//清除填空题上的lineedit上的
{
    ui->lineEdit->clear();
}
void Student::student_sum_grade()//计算分数
{
    int result=0;
    for(int i=0;i<30;i++)
    {
        result=result+grade[i];
    }

    QString string_result=QString::number(result,10);//int型转换成QString型
    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("您本次考试的得分是:\n")+string_result);
    box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.setButtonText (QMessageBox::Cancel,tr("取 消"));
    box.exec ();

    ui->pushButton->setEnabled(true);//点击考试考试后程序某些功能将变不可用
    ui->pushButton_2->setEnabled(true);


    ui->pushButton_5->setEnabled(true);

    ui->pushButton_7->setEnabled(true);
    ui->pushButton_10->setEnabled(true);
    ui->pushButton_11->setEnabled(true);
    ui->pushButton_12->setEnabled(true);
    ui->pushButton_13->setEnabled(true);
    ui->stackedWidget->setCurrentIndex(4);
    QSqlQuery query;
    query.exec("select tno,cno from teacher");
    while(query.next())
    {
        QString str_tno=query.value(0).toString();
        QString str_cno=query.value(1).toString();
        if(str_cno==course_id)
        {

            QSqlQuery query1;
            bool value;
            value=query1.prepare("insert into grade(sno,tno,cno,grades)values(:str_sno,:str_tno,:str_cno,:str_grades)");
            if(value)
            {
                query1.bindValue(":str_sno",sno);
                query1.bindValue(":str_tno",str_tno);
                query1.bindValue(":str_cno",course_id);
                query1.bindValue(":str_grades",result);
                query1.exec();
                return ;
            }
            else {QMessageBox::warning(NULL,tr("系统消息"),tr("成绩插入失败"),QMessageBox::Yes);}

        }
    }



}

void Student::student_put_fill_answer()//提交填空题答案槽函数
{

    if(ui->lineEdit->text()==answer_fill)
    {
         grade[grade_no]=4;
    }
    else grade[grade_no]=0;
    ui->lineEdit->clear();
    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("提交成功！"));
    box.setStandardButtons (QMessageBox::Ok|QMessageBox::Cancel);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.setButtonText (QMessageBox::Cancel,tr("取 消"));
    box.exec ();

}

void Student::student_get_fill_problem()//获取填空题的槽函数
{
    QPushButton *clickedButton = qobject_cast<QPushButton *>(sender());
    ui->stackedWidget_2->setCurrentIndex(2);
    QSqlQuery query;


    query.exec("select * from test_fill");
    while(query.next())
    {
        int cno=query.value(0).toInt();
        QString test_filltext=query.value(1).toString();
        answer_fill=query.value(2).toString();

        if(clickedButton->objectName() == "21")
        {
            if(cno==no_repeat_fill[0])
            {
                test_filltext=test_filltext;
                ui->pushbutton_2_1->setStyleSheet("background-color: rgb(170, 0, 0);");
                Student::student_set_label1(test_filltext);
                grade_no=20;
                return ;
            }
        }
        else if(clickedButton->objectName() == "22")
        {
            if(cno==no_repeat_fill[1])
            {
                ui->pushbutton_2_2->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=21;
                 return ;
            }
        }
        else if(clickedButton->objectName() == "23")
        {
            if(cno==no_repeat_fill[2])
            {
                ui->pushbutton_2_3->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=22;
                return ;
            }
        }
        else if(clickedButton->objectName() == "24")
        {
            if(cno==no_repeat_fill[3])
            {
                ui->pushbutton_2_4->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=23;
                return ;
            }
        }
        else if(clickedButton->objectName() == "25")
        {
            if(cno==no_repeat_fill[4])
            {
                ui->pushbutton_2_5->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=24;
                return ;
            }
        }
        else if(clickedButton->objectName() == "26")
        {
            if(cno==no_repeat_fill[5])
            {
                ui->pushbutton_2_6->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=25;
                return ;
            }
        }
        else if(clickedButton->objectName() == "27")
        {
            if(cno==no_repeat_fill[6])
            {
                ui->pushbutton_2_7->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=26;
                return ;
            }
        }
        else if(clickedButton->objectName() == "28")
        {
            if(cno==no_repeat_fill[7])
            {
                ui->pushbutton_2_8->setStyleSheet("background-color: rgb(170, 0, 0);");
                Student::student_set_label1(test_filltext);
                grade_no=27;
                return ;
            }
        }
        else if(clickedButton->objectName() == "29")
        {
            if(cno==no_repeat_fill[8])
            {
                ui->pushbutton_2_9->setStyleSheet("background-color: rgb(170, 0, 0);");
                 Student::student_set_label1(test_filltext);
                 grade_no=28;
                return ;
            }
        }
        else if(clickedButton->objectName() == "30")
        {
            if(cno==no_repeat_fill[9])
            {
                ui->pushbutton_2_10->setStyleSheet("background-color: rgb(170, 0, 0);");
                Student::student_set_label1(test_filltext);
                grade_no=29;
                return ;
            }
        }
    }
     QMessageBox::warning(NULL,tr("系统消息"),tr("error"),QMessageBox::Yes);
}

void Student::student_get_problem()//获取考试选择题的槽函数
{
    QPushButton *clickedButton1 = qobject_cast<QPushButton *>(sender());
    ui->stackedWidget_2->setCurrentIndex(0);
    QSqlQuery query;
    query.exec("select * from test_choose");
    while(query.next())
    {
        int cno=query.value(0).toInt();
        QString choose_text=query.value(1).toString();
        QString choose_a=query.value(2).toString();
        QString choose_b=query.value(3).toString();
        QString choose_c=query.value(4).toString();
        QString choose_d=query.value(5).toString();
        QString answer=query.value(6).toString();



               if(clickedButton1->objectName() == "1")
               {

                   if(cno==no_repeat[0])
                   {

                       ui->pushbutton_1_1->setStyleSheet("background-color: rgb(170, 0, 0);");
                       Student::student_set_label(0,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                       return ;
                   }
               }

               else if(clickedButton1->objectName() == "2")
               {
                 if(cno==no_repeat[1])
                {
                     ui->pushbutton_1_2->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(1,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "3")
               {
                 if(cno==no_repeat[2])
                {
                     ui->pushbutton_1_3->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(2,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "4")
               {
                 if(cno==no_repeat[3])
                {
                     ui->pushbutton_1_4->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(3,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "5")
               {
                 if(cno==no_repeat[4])
                {
                     ui->pushbutton_1_5->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(4,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "6")
               {
                 if(cno==no_repeat[5])
                {
                     ui->pushbutton_1_6->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(5,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "7")
               {
                 if(cno==no_repeat[6])
                {
                     ui->pushbutton_1_7->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(6,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "8")
               {
                 if(cno==no_repeat[7])
                {
                     ui->pushbutton_1_8->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(7,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "9")
               {
                 if(cno==no_repeat[8])
                {
                     ui->pushbutton_1_9->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(8,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "10")
               {
                 if(cno==no_repeat[9])
                {
                     ui->pushbutton_1_10->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(9,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "11")
               {
                 if(cno==no_repeat[10])
                {
                     ui->pushbutton_1_11->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(10,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "12")
               {
                 if(cno==no_repeat[11])
                {
                     ui->pushbutton_1_12->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(11,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "13")
               {
                 if(cno==no_repeat[12])
                {
                     ui->pushbutton_1_13->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(12,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "14")
               {
                 if(cno==no_repeat[13])
                {
                     ui->pushbutton_1_14->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(13,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "15")
               {
                 if(cno==no_repeat[14])
                {
                     ui->pushbutton_1_15->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(14,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "16")
               {
                 if(cno==no_repeat[15])
                {
                     ui->pushbutton_1_16->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(15,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "17")
               {
                 if(cno==no_repeat[16])
                {
                     ui->pushbutton_1_17->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(16,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "18")
               {
                 if(cno==no_repeat[17])
                {
                     ui->pushbutton_1_18->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(17,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "19")
               {
                 if(cno==no_repeat[18])
                {
                     ui->pushbutton_1_19->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(18,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }
               else if(clickedButton1->objectName() == "20")
               {
                 if(cno==no_repeat[19])
                {
                     ui->pushbutton_1_20->setStyleSheet("background-color: rgb(170, 0, 0);");
                      Student::student_set_label(19,choose_text,choose_a,choose_b, choose_c,choose_d,answer);
                      return ;
                }
               }


    }
      QMessageBox::warning(NULL,tr("系统消息"),tr("error"),QMessageBox::Yes);}

void Student::student_free_talk()//在线交流
{
    ui->stackedWidget->setCurrentIndex(1);

    model->setTable("s_ol");
    model->setSort(0,Qt::DescendingOrder);
    model->setRelation(1,QSqlRelation("student","sno","sname"));
    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("内容:"));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setColumnHidden(0,true);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置单元格为只读
    ui->tableView->setColumnWidth(2,370);

    connect(ui->pushButton_17,SIGNAL(clicked()),this,SLOT(student_free_talk_corret()));
    connect(ui->pushButton_18,SIGNAL(clicked()),this,SLOT(student_free_talk_clear()));

}
void Student::student_t_inform()//教师公告
{
    ui->stackedWidget->setCurrentIndex(2);
    model->setTable("t_inform");
    model->setSort(0,Qt::DescendingOrder);
    model->setRelation(1,QSqlRelation("teacher","tno","tname"));

    model->setHeaderData(1,Qt::Horizontal,tr("教师姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("内容"));
    model->select();

    ui->tableView_2->setModel(model);
    ui->tableView_2->setEditTriggers(QAbstractItemView::NoEditTriggers); //单元格设置为不可编辑
    ui->tableView_2->setColumnHidden(0,true);
    ui->tableView_2->verticalHeader()->hide();
    ui->tableView_2->setColumnWidth(2,415);

}

void Student::student_grade()//查看成绩
{
    ui->stackedWidget->setCurrentIndex(3);
    model->setTable("grade");
    model->setSort(3,Qt::DescendingOrder);
    model->setRelation(0,QSqlRelation("student","sno","sname"));
    model->setRelation(2,QSqlRelation("course","cno","cname"));

    model->setHeaderData(0,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("科目"));
    model->setHeaderData(3,Qt::Horizontal,tr("分数"));
    model->select();
    ui->tableView_3->setModel(model);
    ui->tableView_3->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_3->setColumnHidden(1,true);
    ui->tableView_3->verticalHeader()->hide();

}

Student::~Student()
{
    delete ui;
}

void Student::student_back()//返回到登录界面
{
    this->hide();
    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("安全退出！"));
    box.setStandardButtons (QMessageBox::Ok);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.exec ();
    dui=new Dialog;
    dui->show();

}

void Student::student_close()//返回主页
{
    ui->stackedWidget->setCurrentIndex(4);
}

void Student::student_update_password()//转到修改密码模块
{
    pass=new student_password;
    pass->show();

}

void Student::student_information()//转到个人信息模块
{
    inform=new student_update_information;
    inform->show();
}

void Student::student_help()//转到请求帮助模块
{
    help=new student_need_help;
    help->show();
}



void Student::student_free_talk_corret()//在线交流的某些函数--禁言功能实现
{
    QSqlQuery query1;
    query1.exec("select sno,sspeak from student");
    while(query1.next())
    {
        QString str1=query1.value(0).toString();
        QString str2=query1.value(1).toString();
        if(str1==sno)
        {
            if(str2=="0")
            {
                QString str=ui->textEdit->toPlainText();
                bool value;
                QSqlQuery query;
                value=query.prepare("insert into s_ol(sno,oo)values(:sno,:txt)");
                if(value)
                {
                    query.bindValue(":sno",sno);
                    query.bindValue(":txt",str);
                    query.exec();

                    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("发送成功！"));
                    box.setStandardButtons (QMessageBox::Ok);
                    box.setButtonText (QMessageBox::Ok,tr("确 定"));
                    box.exec ();

                    ui->textEdit->clear();
                    model->setTable("s_ol");
                    model->setSort(0,Qt::DescendingOrder);
                    model->setRelation(1,QSqlRelation("student","sno","sname"));

                    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
                    model->setHeaderData(2,Qt::Horizontal,tr("内容"));
                    model->select();

                    ui->tableView->setModel(model);

                 }
        }

            else
                {

                QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("你已被禁言"));
                box.setStandardButtons (QMessageBox::Ok);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.exec ();

                return ;
            }
    }

    }
}
void Student::student_free_talk_clear()
{
    ui->textEdit->clear();
}

//学生修改密码模块
student_password::student_password(QWidget *parent)
    :QDialog(parent)
{

    QLabel *label_0=new QLabel;//提示密码长度和类型
    label_0->setText(tr("请输入低于15位的字符密码"));

    label_1=new QLabel;
    label_1->setText(tr("旧密码:"));
    LineEdit_1=new QLineEdit;
    label_1->setBuddy(LineEdit_1);
    QHBoxLayout *first_layout=new QHBoxLayout;
    first_layout->addWidget(label_1);
    first_layout->addWidget(LineEdit_1);

    label_2=new QLabel;
    label_2->setText(tr("新密码:"));
    LineEdit_2=new QLineEdit;
    LineEdit_2->setEchoMode(QLineEdit::Password);
    label_2->setBuddy(LineEdit_2);
    QHBoxLayout *second_layout=new QHBoxLayout;
    second_layout->addWidget(label_2);
    second_layout->addWidget(LineEdit_2);

    label_3=new QLabel;
    label_3->setText(tr("新密码:"));
    LineEdit_3=new QLineEdit;
    LineEdit_3->setEchoMode(QLineEdit::Password);
    label_3->setBuddy(LineEdit_3);
    QHBoxLayout *third_layout=new QHBoxLayout;
    third_layout->addWidget(label_3);
    third_layout->addWidget(LineEdit_3);

    QVBoxLayout *top_layout=new QVBoxLayout;
    top_layout->addWidget(label_0);
    top_layout->addLayout(first_layout);
    top_layout->addLayout(second_layout);
    top_layout->addLayout(third_layout);
    top_layout->addStretch();

    button_1=new QPushButton(tr("确定"));
    button_2=new QPushButton(tr("重置"));
    QHBoxLayout *fouth=new QHBoxLayout;
    fouth->addStretch();
    fouth->addWidget(button_1);
    fouth->addWidget(button_2);
    fouth->addStretch();

    connect(button_1,SIGNAL(clicked()),this,SLOT(corret_password()));
    connect(button_2,SIGNAL(clicked()),this,SLOT(clear_text()));

    QVBoxLayout *mainlayout=new QVBoxLayout;
    mainlayout->addLayout(top_layout);
    mainlayout->addLayout(fouth);

    setLayout(mainlayout);
    setWindowTitle(tr("密码修改"));
    setFixedHeight(sizeHint().height());
}

void student_password::corret_password()
{
    QSqlQuery query;
    query.exec("SELECT sno,spassword FROM student ");
    while(query.next())
    {
    if((query.value(0).toString()==sno)&&(LineEdit_1->text()==query.value(1).toString()))//旧密码和数据库中的密码匹配
    {
        if((LineEdit_2->text().length()==LineEdit_3->text().length())&&(LineEdit_2->text().length()<=15))
        {
            bool value;
            QString pword;
            pword=LineEdit_2->text();
            QSqlQuery query0;
            value=query0.prepare("UPDATE student SET spassword=:ipassword WHERE sno=:ino");
            if(value)
            {
                query0.bindValue("ipassword",LineEdit_2->text());
                query0.bindValue("ino",sno);
                query0.exec();
                QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("修改成功！"));
                box.setStandardButtons (QMessageBox::Ok);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.exec();
                return ;
            }
            else
            {
                QMessageBox::information(NULL," ",tr("密码修改失败!"),QMessageBox::Yes);
                return ;
            }


        }

        else if((LineEdit_2->text().length()==LineEdit_3->text().length())&&(LineEdit_2->text().length()>15))
        {
            QMessageBox::critical(NULL,tr("系统消息"),tr("密码长度大于15位"),QMessageBox::Yes);
            LineEdit_2->clear();
            LineEdit_3->clear();
            LineEdit_2->setFocus();
            return ;
        }
        else if(LineEdit_2->text().length()!=LineEdit_3->text().length())
        {
            QMessageBox::critical(NULL,tr("错误"),tr("两次密码不一致"),QMessageBox::Yes);
            LineEdit_2->clear();
            LineEdit_3->clear();
            LineEdit_2->setFocus();
            return ;
        }
    }
    }

        QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("旧密码输入错误！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec();

        LineEdit_1->clear();
        LineEdit_2->clear();
        LineEdit_3->clear();
        LineEdit_1->setFocus();

}

void student_password::clear_text()
{
    LineEdit_1->clear();
    LineEdit_2->clear();
    LineEdit_3->clear();
    LineEdit_1->setFocus();
}
student_password::~student_password()
{


}

//学生信息修改模块
student_update_information::student_update_information(QWidget *parent)
    :QDialog(parent)
{
    QSqlQuery query;
    query.exec("select sno,sname,ssex,sdept,sclass,stel,cno1,cno2 from student ");
    while(query.next())
    {
        QString stsno=query.value(0).toString();

        if(stsno==sno)
        {

            QString ssname=query.value(1).toString();
            QString sssex=query.value(2).toString();
            QString ssdept=query.value(3).toString();
            QString sclass=query.value(4).toString();
            QString sstel=query.value(5).toString();
            QString cno1=query.value(6).toString();
            QString cno2=query.value(7).toString();

            student_id=new QLabel(tr("学号:"));
            LineEdit_id=new QLineEdit;
            LineEdit_id->setText(sno);
            LineEdit_id->setFocusPolicy(Qt::NoFocus);
            student_id->setBuddy(LineEdit_id);
            QHBoxLayout *layout_id=new QHBoxLayout;
            layout_id->addWidget(student_id);
            layout_id->addWidget(LineEdit_id);

            student_name=new QLabel(tr("姓名:"));
            LineEdit_name=new QLineEdit;
            LineEdit_name->setText(ssname);
            student_name->setBuddy(LineEdit_name);
            QHBoxLayout *layout_name=new QHBoxLayout;
            layout_name->addWidget(student_name);
            layout_name->addWidget(LineEdit_name);

            student_sex=new QLabel(tr("性别:"));
            ComboBox_sex=new QComboBox;
            ComboBox_sex->addItem(QWidget::tr("男"));
            ComboBox_sex->addItem(QWidget::tr("女"));
            if(sssex==tr("男"))
                {ComboBox_sex->setCurrentIndex(0);}
            else {ComboBox_sex->setCurrentIndex(1);}
            student_sex->setBuddy(ComboBox_sex);
            QHBoxLayout *layout_sex=new QHBoxLayout;
            layout_sex->addWidget(student_sex);
            layout_sex->addWidget(ComboBox_sex);
            layout_sex->addStretch();

            student_department=new QLabel(tr("学院:"));
            LineEdit_department=new QLineEdit;
            LineEdit_department->setText(ssdept);
            LineEdit_department->setFocusPolicy(Qt::NoFocus);
            student_department->setBuddy(LineEdit_department);
            QHBoxLayout *layout_department=new QHBoxLayout;
            layout_department->addWidget(student_department);
            layout_department->addWidget(LineEdit_department);

            student_class=new QLabel(tr("班级:"));
            LineEdit_class=new QLineEdit;
            LineEdit_class->setText(sclass);
            LineEdit_class->setFocusPolicy(Qt::NoFocus);
            student_class->setBuddy(LineEdit_class);
            QHBoxLayout *layout_class=new QHBoxLayout;
            layout_class->addWidget(student_class);
            layout_class->addWidget(LineEdit_class);

            student_tel=new QLabel(tr("电话:"));
            LineEdit_tel=new QLineEdit;
            LineEdit_tel->setText(sstel);
            student_tel->setBuddy(LineEdit_tel);
            QHBoxLayout *layout_tel=new QHBoxLayout;
            layout_tel->addWidget(student_tel);
            layout_tel->addWidget(LineEdit_tel);

            QVBoxLayout *top_layout=new QVBoxLayout;
            top_layout->addLayout(layout_id);
            top_layout->addLayout(layout_name);
            top_layout->addLayout(layout_sex);
            top_layout->addLayout(layout_class);
            top_layout->addLayout(layout_department);
            top_layout->addLayout(layout_tel);

            button_1=new QPushButton(tr("提交"));
            button_2=new QPushButton(tr("重置"));
            QHBoxLayout *layout_button=new QHBoxLayout;
            layout_button->addStretch();
            layout_button->addWidget(button_1);
            layout_button->addWidget(button_2);
            layout_button->addStretch();

            QVBoxLayout *mainlayout=new QVBoxLayout;
            mainlayout->addLayout(top_layout);
            mainlayout->addLayout(layout_button);


            setLayout(mainlayout);
            setWindowTitle(tr("信息修改"));

            this->setFixedSize(200,300);

            connect(button_1,SIGNAL(clicked()),this,SLOT(to_sumitall()));
            connect(button_2,SIGNAL(clicked()),this,SLOT(to_clear_all()));
            connect(LineEdit_name,SIGNAL(textChanged(const QString &)),this,SLOT(enable_button(const QString &)));

            return ;
        }
    }

}
void student_update_information::to_sumitall()
{
    bool value;
    QSqlQuery query;
    value=query.prepare("UPDATE student SET sname=:iname,ssex=:isex,stel=:itel WHERE sno=:ino");
    if(value)
    {
        query.bindValue("iname",LineEdit_name->text());
        query.bindValue("isex",ComboBox_sex->currentText());
        query.bindValue("itel",LineEdit_tel->text());
        query.bindValue("ino",sno);
        query.exec();

        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("信息修改成功！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();

        return ;
    }

}
void student_update_information::to_clear_all()
{
    LineEdit_name->clear();
    LineEdit_tel->clear();
}

void student_update_information::enable_button(const QString &text)
{
     button_1->setEnabled(!text.isEmpty());
}

student_update_information::~student_update_information()
{

}

//学生请求帮助模块
student_need_help::student_need_help(QWidget *parent)
    :QDialog(parent)
{
    label=new QLabel;
    label->setText(tr("请您输入少于128个字的请求："));

    textedit=new QTextEdit;
    QVBoxLayout *top_layout=new QVBoxLayout;
    top_layout->addWidget(label);
    top_layout->addWidget(textedit);

    button1=new QPushButton;
    button1->setText(tr("提交"));
    button2=new QPushButton;
    button2->setText(tr("清空"));
    QHBoxLayout *layout_button=new QHBoxLayout;
    layout_button->addStretch();
    layout_button->addWidget(button1);
    layout_button->addWidget(button2);
    layout_button->addStretch();

    QVBoxLayout *mainlayout=new QVBoxLayout;
    mainlayout->addLayout(top_layout);
    mainlayout->addLayout(layout_button);

    setLayout(mainlayout);
    setWindowTitle(tr("请求帮助"));

    connect(button1,SIGNAL(clicked()),this,SLOT(student_summitall()));
    connect(button2,SIGNAL(clicked()),this,SLOT(student_clear_all()));

}

void student_need_help::student_clear_all()
{
    textedit->clear();
}

void student_need_help::student_summitall()
{
    QString str=textedit->toPlainText();
    bool value;
    QSqlQuery query;
    value=query.prepare("insert into s_request(sno,neirong) values(:sno,:txt)");
    if(value)
    {
        query.bindValue(":sno",sno);
        query.bindValue(":txt",str);
        query.exec();

        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("请求已发送！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
        this->close();

        return ;

    }
}

student_need_help::~student_need_help()
{

}
