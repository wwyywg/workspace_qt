#ifndef TEACHER_H
#define TEACHER_H

#include <QWidget>
#include<QTimer>
#include<QDateTime>
#include"dialog.h"
#include<QSqlRelationalTableModel>
#include<QTextEdit>
#include<QSqlQueryModel>
#include<QList>

namespace Ui {
class Teacher;
}

class teacher_need_help:public QDialog//学生请求帮助模块
{
    Q_OBJECT

public:
    teacher_need_help(QWidget *parent=0);
    ~teacher_need_help();
private slots:
    void teacher_clear_all();
    void teacher_summitall();

private:
    QLabel *label;
    QTextEdit *textedit;
    QPushButton *button1;
    QPushButton *button2;


};

class Teacher : public QWidget
{
    Q_OBJECT
    
public:
    explicit Teacher(QWidget *parent = 0);
    ~Teacher();
    
private slots:
    void teacher_update_time();
    void teacher_self();
    void teacher_change_password();
    void teacher_grade_manage();
    void teacher_problem_manage();
    void teacher_manage_test();
    void teacher_inform();
    void teacher_talk_online();
    void teacher_look_for_help();
    void teacher_close();

    void change_password_summit();//提交新密码
    void change_password_clear();//清除密码输入框

    void problem_check();//试题查询--模糊匹配
    void problem_f5();//刷新试题
    void problem_delete();//删除试题
    void problem_add();//添加试题
    void problem_summit_all();//提交记录
    void problem_update();//确认修改

    void test_start();//发起考试
    void test_cancel();//撤销考试
    void text_change(int row, int col);//选择题复选框被选的情况
    void text_change_fill(int row,int col);//填空题复选框被选的情况
    void submmit_choose_problem();//提交选中的选择题
    void submmit_fill_problem();//提交选中的填空题

    void submmit_inform();//发布公告
    void clear_inform();//清除公告内容

    void free_talk_submmit();//发送聊天内容
    void clear_talk();//清除聊天输入框


private:
    Ui::Teacher *ui;
    teacher_need_help *help;
    QTimer *teacher_timer;
    QSqlRelationalTableModel *model;
    QSqlRelationalTableModel *model1;
    QString course_no;
    QSqlQueryModel *model2;
    int choose_choose;
    int choose_fill;

};

#endif // TEACHER_H
