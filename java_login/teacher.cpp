#include "teacher.h"
#include "ui_teacher.h"
#include<QString>
#include<QPushButton>
#include<QLabel>
#include<QSqlQuery>
#include<QSqlRecord>
#include <QHeaderView>
#include<QDebug>
#include<QTableWidget>
#include<QTableWidgetItem>



Teacher::Teacher(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Teacher)
{
    ui->setupUi(this);
    setWindowTitle(tr("教师客户端"));//设置窗口标题
    this->setFixedSize( this->width (),this->height ());//固定窗口大小
    ui->stackedWidget->setCurrentIndex(0);

    model=new QSqlRelationalTableModel(this);
    model1=new QSqlRelationalTableModel(this);
    model2=new QSqlQueryModel;
    choose_choose=0;
    choose_fill=0;
    //设置来自管理员的消息
    QSqlQuery query1;
    query1.exec("select text from admini_say");
    while(query1.last())
    {
        QString str=query1.value(0).toString();
        QMessageBox box(QMessageBox::Information,tr("管理员消息"),str+tr("\n"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec();
        break;
    }

    //设置欢迎您标签
    QSqlQuery query;
    query.exec("select tno,tname,tsex from teacher");
    while(query.next())
    {
        QString teacher_tno=query.value(0).toString();
        QString teacher_tname=query.value(1).toString();
        QString teacher_tsex=query.value(2).toString();
        if(teacher_tno==tno)
        {
           ui->label_47->setText(teacher_tname);
           ui->label_48->setText(teacher_tno);
           ui->label_49->setText(teacher_tsex);
           break;
        }
    }

    //设置左下角的时间
    ui->tabWidget->setCurrentIndex(0);
    //qDebug()<<ui->tableView_2->currentIndex().row();
    teacher_timer=new QTimer(this);
    teacher_timer->start(1000);

    connect(teacher_timer,SIGNAL(timeout()),this,SLOT(teacher_update_time()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(teacher_self()));
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(teacher_change_password()));
    connect(ui->pushButton_6,SIGNAL(clicked()),this,SLOT(teacher_grade_manage()));
    connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(teacher_problem_manage()));
    connect(ui->pushButton_5,SIGNAL(clicked()),this,SLOT(teacher_manage_test()));
    connect(ui->pushButton_9,SIGNAL(clicked()),this,SLOT(teacher_inform()));
    connect(ui->pushButton_8,SIGNAL(clicked()),this,SLOT(teacher_talk_online()));
    connect(ui->pushButton_4,SIGNAL(clicked()),this,SLOT(teacher_close()));
    connect(ui->pushButton_7,SIGNAL(clicked()),this,SLOT(teacher_look_for_help()));

}

void Teacher::teacher_update_time()//设置左下角的时间
{
    QDateTime time=QDateTime::currentDateTime();
    QString str=time.toString("yyyy-MM-dd hh:mm:ss dddd");
    ui->label_16->setText(str);
}
void Teacher::teacher_self()//教师个人信息
{
    ui->stackedWidget->setCurrentIndex(1);
    QSqlQuery query;
    query.exec("select * from teacher");
    while(query.next())
    {
        QString teacher_no=query.value(0).toString();//教工号
        QString teacher_name=query.value(1).toString();//教师名
        QString teacher_sex=query.value(2).toString();//性别
        QString teacher_tel=query.value(4).toString();//手机号码
        QString teacher_cno=query.value(5).toString();//主讲课程
        QString teacher_dept=query.value(7).toString();//部门
        QString teacher_job=query.value(8).toString();//职位
        QString teacher_blood=query.value(9).toString();//血型
        if(teacher_no==tno)
        {
            QSqlQuery query1;
            query1.exec("select cno,cname from course");//获取课程名
            while(query1.next())
          {
            QString course_cno=query1.value(0).toString();
            QString course_name=query1.value(1).toString();
            if(course_cno==teacher_cno)
            {
                ui->label_1_1->setText(tno);
                ui->label_1_2->setText(teacher_name);
                ui->label_1_3->setText(teacher_sex);
                ui->label_1_4->setText(teacher_job);
                ui->label_1_5->setText(course_name);
                ui->label_1_6->setText(teacher_tel);
                ui->label_1_7->setText(teacher_dept);
                ui->label_1_8->setText(teacher_blood);
                return ;

            }
          }

        }

    }
}
void Teacher::teacher_change_password()//教师修改密码
{
   ui->stackedWidget->setCurrentIndex(7);
   connect(ui->pushButton_16,SIGNAL(clicked()),this,SLOT(change_password_summit()));
   connect(ui->pushButton_17,SIGNAL(clicked()),this,SLOT(change_password_clear()));
   ui->lineEdit_2->setEchoMode(QLineEdit::Password);
   ui->lineEdit_3->setEchoMode(QLineEdit::Password);

}
   void Teacher::change_password_summit()//提交修改的新密码
{
    QSqlQuery query;
    query.exec("select tno,tpassword from teacher");
    while(query.next())
    {
    if((query.value(0).toString()==tno)&&(ui->lineEdit->text()==query.value(1).toString()))//旧密码和数据库中的密码匹配
    {
        if((ui->lineEdit_2->text().length()==ui->lineEdit_3->text().length())&&(ui->lineEdit_2->text().length()<=15)&&(ui->lineEdit_2->text()==ui->lineEdit_3->text()))
        {
            bool value;
            QString pword;
            pword=ui->lineEdit_2->text();
            QSqlQuery query0;
            value=query0.prepare("UPDATE teacher SET tpassword=:ipassword WHERE tno=:ino");
            if(value)
            {
                query0.bindValue("ipassword",ui->lineEdit_2->text());
                query0.bindValue("ino",tno);
                query0.exec();
                QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("修改成功！"));
                box.setStandardButtons (QMessageBox::Ok);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.exec();
                ui->lineEdit->clear();
                ui->lineEdit_2->clear();
                ui->lineEdit_3->clear();
                ui->stackedWidget->setCurrentIndex(0);
                return ;
            }
            else
            {
                QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("修改失败！"));
                box.setStandardButtons (QMessageBox::Ok);
                box.setButtonText (QMessageBox::Ok,tr("确 定"));
                box.exec();
                return ;
            }


        }

        else if((ui->lineEdit_2->text().length()==ui->lineEdit_3->text().length())&&(ui->lineEdit_2->text().length()>15))
        {
            QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("密码长度大于15位"));
            box.setStandardButtons (QMessageBox::Ok);
            box.setButtonText (QMessageBox::Ok,tr("确 定"));
            box.exec();
            ui->lineEdit_2->clear();
            ui->lineEdit_3->clear();
            ui->lineEdit_2->setFocus();
            return ;
        }
        else if(ui->lineEdit_2->text().length()!=ui->lineEdit_3->text().length()||ui->lineEdit_2->text()!=ui->lineEdit_3->text())
        {
            QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("两次新密码输入不一致"));
            box.setStandardButtons (QMessageBox::Ok);
            box.setButtonText (QMessageBox::Ok,tr("确 定"));
            box.exec();
            ui->lineEdit_2->clear();
            ui->lineEdit_3->clear();
            ui->lineEdit_2->setFocus();
            return ;
        }
    }
    }

        QMessageBox box(QMessageBox::Critical,tr("系统消息"),tr("旧密码输入错误！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec();

        ui->lineEdit->clear();
        ui->lineEdit_2->clear();
        ui->lineEdit_3->clear();
        ui->lineEdit->setFocus();
}
   void Teacher::change_password_clear()//清除密码输入框
{
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->lineEdit->setFocus();
}

void Teacher::teacher_grade_manage()//教师成绩管理
{
    ui->stackedWidget->setCurrentIndex(2);
    //model2->setTable("grade");
    //qDebug()<<tno;
    model2->setQuery(tr("SELECT sno,tno,cno,grades FROM grade WHERE grade.tno='%1'").arg(tno));
    //model2->setSort(3,Qt::DescendingOrder);//按成绩从大到小排列
    //model2->setRelation(2,QSqlRelation("course","cno","cname"));
    model2->setHeaderData(0,Qt::Horizontal,tr("学号"));
    model2->setHeaderData(1,Qt::Horizontal,tr("教工号"));
    model2->setHeaderData(2,Qt::Horizontal,tr("课程名"));
    model2->setHeaderData(3,Qt::Horizontal,tr("分数"));
    //model2->select();
    //model2->setEditStrategy(QSqlTableModel::OnManualSubmit);//数据修改后，收到提交才能生效

    ui->tableView->setModel(model2);
    ui->tableView->verticalHeader()->hide();


}


void Teacher::teacher_problem_manage()//试题管理
{
    ui->stackedWidget->setCurrentIndex(3);
    QSqlQuery query;
    query.exec("SELECT teacher.tno,course.cname FROM teacher,course WHERE course.tno = teacher.tno AND teacher.cno = course.cno");
    while(query.next())
    {
        QString teacher_tno=query.value(0).toString();
        QString course_name=query.value(1).toString();
        if(teacher_tno==tno)
        {
            ui->label_31->setText(course_name+tr("试题管理"));
            break;
        }
    }
    //选择题显示
    model->setTable("test_choose");
    model->setSort(0,Qt::AscendingOrder);
    model->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model->setHeaderData(1,Qt::Horizontal,tr("题目"));
    model->setHeaderData(2,Qt::Horizontal,tr("选项A"));
    model->setHeaderData(3,Qt::Horizontal,tr("选项B"));
    model->setHeaderData(4,Qt::Horizontal,tr("选项C"));
    model->setHeaderData(5,Qt::Horizontal,tr("选项D"));
    model->setHeaderData(6,Qt::Horizontal,tr("答案"));
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);//数据修改后，收到提交才能生效
    model->select();
    ui->tableView_2->setModel(model);
    ui->tableView_2->verticalHeader()->hide();
    ui->tableView_2->setSelectionBehavior(QAbstractItemView::SelectRows);

    //填空题显示
    model1->setTable("test_fill");
    model1->setSort(0,Qt::AscendingOrder);
    model1->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model1->setHeaderData(1,Qt::Horizontal,tr("题目"));
    model1->setHeaderData(2,Qt::Horizontal,tr("答案"));
    model1->select();
    model1->setEditStrategy(QSqlTableModel::OnManualSubmit);//数据修改后，收到提交才能生效
    ui->tableView_3->setModel(model1);
    ui->tableView_3->verticalHeader()->hide();
    ui->tableView_3->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->tableView_3->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(ui->pushButton_23,SIGNAL(clicked()),this,SLOT(problem_check()));
    connect(ui->pushButton_24,SIGNAL(clicked()),this,SLOT(problem_f5()));
    connect(ui->pushButton_21,SIGNAL(clicked()),this,SLOT(problem_delete()));
    connect(ui->pushButton_20,SIGNAL(clicked()),this,SLOT(problem_add()));
    connect(ui->pushButton_22,SIGNAL(clicked()),this,SLOT(problem_summit_all()));
    connect(ui->pushButton_25,SIGNAL(clicked()),this,SLOT(problem_update()));
}
void Teacher::problem_check()//试题查询--模糊匹配
{
    QString filter = ui->lineEdit_4->text();
    if(ui->lineEdit_4->text().isEmpty())
    {
        return ;
    }
    QSqlRecord record;
    if(ui->tabWidget->currentIndex()==0)
    {
    record=model->record();
    }
    else {record=model1->record();}
    QString modelfilter;
    for(int i=0;i<record.count();++i)
    {

        if(i!=0)
        {
            modelfilter +=" or ";
        }
        QString fied=record.fieldName(i);
        QString subfilter=QString().sprintf("%s like '%%%s%%'", fied.toUtf8().data(),filter.toUtf8().data());

        modelfilter +=subfilter;
    }
    if(ui->tabWidget->currentIndex()==0)
    {
    model->setFilter(modelfilter);
    model->select();
    }
    else{ model1->setFilter(modelfilter);
        model1->select();
    }
}

void Teacher::problem_f5()//刷新试题
{
    //设置选择题显示
    model->setTable("test_choose");
    model->setSort(0,Qt::AscendingOrder);
    model->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model->setHeaderData(1,Qt::Horizontal,tr("题目"));
    model->setHeaderData(2,Qt::Horizontal,tr("选项A"));
    model->setHeaderData(3,Qt::Horizontal,tr("选项B"));
    model->setHeaderData(4,Qt::Horizontal,tr("选项C"));
    model->setHeaderData(5,Qt::Horizontal,tr("选项D"));
    model->setHeaderData(6,Qt::Horizontal,tr("答案"));
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);//数据修改后，收到提交才能生效*/
    model->select();
    ui->tableView_2->setModel(model);
    ui->tableView_2->verticalHeader()->hide();
    ui->tableView_2->setSelectionBehavior(QAbstractItemView::SelectRows);


    //设置填空题显示
    model1->setTable("test_fill");
    model1->setSort(0,Qt::AscendingOrder);
    model1->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model1->setHeaderData(1,Qt::Horizontal,tr("题目"));
    model1->setHeaderData(2,Qt::Horizontal,tr("答案"));
    model1->setEditStrategy(QSqlTableModel::OnManualSubmit);//数据修改后，收到提交才能生效
    model1->select();
    ui->tableView_3->setModel(model1);
    ui->tableView_3->verticalHeader()->hide();
    ui->tableView_3->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->tableView_3->setSelectionBehavior(QAbstractItemView::SelectRows);

}

void Teacher::problem_delete()//删除试题
{
    int curRow;

    if(ui->tabWidget->currentIndex()==0)
    {
        if(ui->tableView_2->currentIndex().row()!=-1)
        {
        curRow=ui->tableView_2->currentIndex().row();
        int ok=QMessageBox::warning(NULL,tr("系统消息"),tr("确定删除当前行吗？"),QMessageBox::Yes|QMessageBox::No);
        if(ok==QMessageBox::No)
        {
           return ;
        }
        else
        {
            model->removeRow(curRow);
            model->submitAll();
            QMessageBox::information(NULL,tr("系统消息"),tr("删除成功"),QMessageBox::Yes);
            return ;
        }
        }
        else {QMessageBox::warning(NULL,tr("系统消息"),tr("没有选中的行"),QMessageBox::Yes);return ;}

    }
    else
    {
         if(ui->tableView_3->currentIndex().row()!=-1)
         {
        curRow=ui->tableView_3->currentIndex().row();

        int ok=QMessageBox::warning(NULL,tr("系统消息"),tr("确定删除当前行吗？"),QMessageBox::Yes|QMessageBox::No);
        if(ok==QMessageBox::No)
        {
           return ;
        }
        else
        {
            model1->removeRow(curRow);
            model1->submitAll();
            QMessageBox::information(NULL,tr("系统消息"),tr("删除成功"),QMessageBox::Yes);
            return ;
        }
         }
        else {QMessageBox::warning(NULL,tr("系统消息"),tr("没有选中的行"),QMessageBox::Yes);}
    }

}

void Teacher::problem_add()//添加试题
{
    int rownum;
    if(ui->tabWidget->currentIndex()==0)
    {
        rownum=model->rowCount();
        model->insertRow(rownum);
        //model->submitAll();
        return ;
    }

    else if(ui->tabWidget->currentIndex()==1)
    {
        rownum=model1->rowCount();
        model1->insertRow(rownum);
        //model1->submitAll();
        return ;
    }
}
   void Teacher::problem_summit_all()//提交记录
{
     if(ui->tabWidget->currentIndex()==0)
     {
         QMessageBox::StandardButton ab=QMessageBox::warning(NULL,tr("系统消息"),tr("确认插入记录？"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
         if(ab==QMessageBox::Yes)
         {
            if(model->submitAll()==1)
            {
             QMessageBox::information(NULL,tr("系统消息"),tr("添加记录成功"),QMessageBox::Yes);
             return ;
            }
         }
         else return ;

     }
     else
     {
         QMessageBox::StandardButton ab=QMessageBox::warning(NULL,tr("系统消息"),tr("确认插入记录？"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
         if(ab==QMessageBox::Yes)
         {
            model1->submitAll();
             QMessageBox::information(NULL,tr("系统消息"),tr("添加记录成功"),QMessageBox::Yes);
         }
         else return ;

     }

}

void Teacher::problem_update()//确认修改
{
    if(ui->tabWidget->currentIndex()==0)
    {
    model->database().transaction();
    if(model->submitAll())
    {
        model->database().commit();
        QMessageBox::information(NULL,tr("系统消息"),tr("修改成功"),QMessageBox::Yes);
        return ;
    }

    else
    {
        model->database().rollback();
        QMessageBox::critical(NULL,tr("系统消息"),tr("修改失败"),QMessageBox::Yes);
        return ;
    }
    }

    else
    {
        model1->database().transaction();
        if(model1->submitAll())
        {
            model1->database().commit();
            QMessageBox::information(NULL,tr("系统消息"),tr("修改成功"),QMessageBox::Yes);
            return ;
        }

        else
        {
            model1->database().rollback();
            QMessageBox::critical(NULL,tr("系统消息"),tr("修改失败"),QMessageBox::Yes);
            return ;
        }

    }
}

void Teacher::teacher_manage_test()//教师发起考试
{
   ui->stackedWidget->setCurrentIndex(4);
   ui->tabWidget_2->setCurrentIndex(0);
   if(ui->tabWidget_2->currentIndex()==0)
   {
   //设置发起考试

   QSqlQuery query;
   query.exec("SELECT\
              course.cname,\
              teacher.tno,\
              course.cno\
              FROM\
              course,teacher\
              WHERE  teacher.cno = course.cno AND course.tno = teacher.tno");
   while(query.next())
   {
           QString teacher_cname=query.value(0).toString();
           QString teacher_no=query.value(1).toString();
                   course_no=query.value(2).toString();
   if(tno==teacher_no)
   {
       ui->label_35->setText(teacher_cname);

       break;
   }

   }


   ui->comboBox_1->addItem(tr("02:00:00"));

   QSqlQuery query1;
   query1.exec("SELECT teacher.tno,student.sclass FROM teacher ,student\
               WHERE teacher.cno=student.cno1 or teacher.cno=student.cno2");
   while(query1.next())
   {
           QString teacher_no=query1.value(0).toString();
           QString student_class=query1.value(1).toString();
         if(tno==teacher_no)
         {
            ui->comboBox->addItem(student_class);
         }
   }

   connect(ui->pushButton_10,SIGNAL(clicked()),this,SLOT(test_start()));
   connect(ui->pushButton_11,SIGNAL(clicked()),this,SLOT(test_cancel()));
}

   //选择题显示
   QSqlQuery query2;
   query2.exec("select * from test_choose");
   int sum=0;
   while(query2.next())
   {
       sum++;
   }
   ui->tableWidget->setRowCount(sum);
   ui->tableWidget->setColumnCount(8);
   QStringList header;
   header<<tr("选中")<<tr("序号")<<tr("题目")<<tr("选项A")<<tr("选项B")<<tr("选项C")<<tr("选项D")<<tr("答案");
   ui->tableWidget->setHorizontalHeaderLabels(header);
   for(int i=0;i<sum;i++)
   {
       QTableWidgetItem *checkBox = new QTableWidgetItem();
       checkBox->setCheckState(Qt::Unchecked);
       ui->tableWidget->setItem(i,0,checkBox);
   }
   qDebug()<<sum;
   QString str_sum=QString::number(sum,10);
   ui->label_51->setText(str_sum);
   QSqlQuery query3;
   query3.exec("select * from test_choose");
   int j,k;
   j=0;k=1;
   while(query3.next())
   {
       k=1;
       QString str_no=query3.value(0).toString();
       QString str_text=query3.value(1).toString();
       QString str_a=query3.value(2).toString();
       QString str_b=query3.value(3).toString();
       QString str_c=query3.value(4).toString();
       QString str_d=query3.value(5).toString();
       QString str_answer=query3.value(6).toString();
       ui->tableWidget->setItem(j,k,new QTableWidgetItem(str_no));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_text));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_a));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_b));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_c));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_d));
       ui->tableWidget->setItem(j,++k,new QTableWidgetItem(str_answer));
       j++;
   }
   ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
   ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//不可编辑
   ui->tableWidget->setSelectionMode(QAbstractItemView::MultiSelection);//可以选中多行
   ui->tableWidget->setStyleSheet("selection-background-color:lightblue;");//设置选中行的颜色
   ui->tableWidget->verticalHeader()->setVisible(false);//隐藏列表头
   //qDebug()<<choose_choose;
   connect(ui->tableWidget,SIGNAL(cellChanged(int,int)),this,SLOT(text_change(int,int)));

   //填空题显示
   QSqlQuery query4;
   query4.exec("select * from test_fill");
   int fill_sum=0;
   while(query4.next())
   {
       fill_sum++;
   }
   ui->tableWidget_2->setRowCount(fill_sum);
   ui->tableWidget_2->setColumnCount(4);
   header<<tr("选中")<<tr("序号")<<tr("题目")<<tr("答案");
   ui->tableWidget_2->setHorizontalHeaderLabels(header);
   for(int i=0;i<fill_sum;i++)
   {
       QTableWidgetItem *checkBox = new QTableWidgetItem();
       checkBox->setCheckState(Qt::Unchecked);
       ui->tableWidget_2->setItem(i,0,checkBox);
   }
   str_sum=QString::number(fill_sum,10);
   ui->label_55->setText(str_sum);
   QSqlQuery query5;
   query5.exec("select * from test_fill");
   j=0;k=1;
   while(query5.next())
   {
       k=1;
       QString str_no=query5.value(0).toString();
       QString str_text=query5.value(1).toString();
       QString str_answer=query5.value(2).toString();
       ui->tableWidget_2->setItem(j,k,new QTableWidgetItem(str_no));
       ui->tableWidget_2->setItem(j,++k,new QTableWidgetItem(str_text));
       ui->tableWidget_2->setItem(j,++k,new QTableWidgetItem(str_answer));
       j++;
   }
   ui->tableWidget_2->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
   ui->tableWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers);//不可编辑
   ui->tableWidget_2->setSelectionMode(QAbstractItemView::MultiSelection);//可以选中多行
   ui->tableWidget_2->setStyleSheet("selection-background-color:lightblue;");//设置选中行的颜色
   ui->tableWidget_2->verticalHeader()->setVisible(false);//隐藏列表头
   connect(ui->tableWidget_2,SIGNAL(cellChanged(int,int)),this,SLOT(text_change_fill(int,int)));
   connect(ui->pushButton_18,SIGNAL(clicked()),this,SLOT(submmit_choose_problem()));
   connect(ui->pushButton_19,SIGNAL(clicked()),this,SLOT(submmit_fill_problem()));
}
//选择题复选框情况
void Teacher::text_change(int row, int col)
{

    if(ui->tableWidget->item(row,col)->checkState()==Qt::Checked)//复选框被勾选的情况
    {

       ui->tableWidget->setCurrentCell(row,QItemSelectionModel::Select);
       choose_choose=choose_choose+1;
       QString str_choose=QString("%1").arg(choose_choose);
       ui->label_13->setText(str_choose);
       bool value;
       QSqlQuery query;
       value=query.prepare("insert into no_problem(problem_no) values(:sno)");
       if(value)
       {
           query.bindValue(":sno",row+1);
           query.exec();
       }

       //qDebug()<<choose_choose;
       //qDebug()<<"001";

    }
    else if(ui->tableWidget->item(row,col)->checkState()==Qt::Unchecked)//复选框被取消的情况
    {
        choose_choose=choose_choose-1;
        QString str_choose=QString("%1").arg(choose_choose);
        ui->label_13->setText(str_choose);
        QSqlQuery query;
        bool value;
        value=query.prepare("delete from no_problem where problem_no=:str_sno ");
        if(value)
        {
            query.bindValue(":str_sno",row+1);
            query.exec();
        }
        //if(choose_choose<0) choose_choose++;
        //qDebug()<<choose_choose;
        //qDebug()<<"002";

    }


}
//填空题复选框情况
void Teacher::text_change_fill(int row, int col)
{
    if(ui->tableWidget_2->item(row,col)->checkState()==Qt::Checked)//复选框被勾选的情况
    {

       ui->tableWidget_2->setCurrentCell(row,QItemSelectionModel::Select);
       choose_fill=choose_fill+1;
       QString str_choose=QString("%1").arg(choose_fill);
       ui->label_57->setText(str_choose);
       bool value;
       QSqlQuery query;
       value=query.prepare("insert into fill_problem_no(problem_no) values(:sno)");
       if(value)
       {
           query.bindValue(":sno",row+1);
           query.exec();
       }

      }
    else if(ui->tableWidget_2->item(row,col)->checkState()==Qt::Unchecked)//复选框被取消的情况
    {
        choose_fill=choose_fill-1;
        QString str_choose=QString("%1").arg(choose_fill);
        ui->label_57->setText(str_choose);
        QSqlQuery query;
        bool value;
        value=query.prepare("delete from fill_problem_no where problem_no=:str_sno ");
        if(value)
        {
            query.bindValue(":str_sno",row+1);
            query.exec();
        }

    }
}
//提交选中的选择题
void Teacher::submmit_choose_problem()
{
    if(choose_choose==20)
    {
        bool value;
        QSqlQuery query;
        value=query.prepare("UPDATE no_problem SET problem_no=:ipassword WHERE no=:ino");
        if(value)
        {
            query.bindValue("ipassword",0);
            query.bindValue("ino",1);
            query.exec();
        }
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("提交考试 选择题 成功！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
    else if(choose_choose<20)
    {
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("选中的选择题题少于要求的数目，请继续选题！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
    else
    {
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("选中的选择题多于要求的数目，请减少选中的试题！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
}
//提交选中的填空题
void Teacher::submmit_fill_problem()
{
    if(choose_fill==10)
    {
        bool value;
        QSqlQuery query;
        value=query.prepare("UPDATE fill_problem_no SET problem_no=:ipassword WHERE no=:ino");
        if(value)
        {
            query.bindValue("ipassword",0);
            query.bindValue("ino",1);
            query.exec();
        }
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("提交考试 填空题 成功！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
    else if(choose_fill<10)
    {
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("选中的填空题少于要求的数目，请继续选题！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
    else
    {
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("选中的填空题多于要求的数目，请减少选中的选题！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }
}

void Teacher::test_start()//发起考试
{


    QString student_class=ui->comboBox->currentText();
    //检查数据库中是否已有该班的考试记录--避免重复数据
    QSqlQuery query1;
    query1.exec("SELECT student.sclass,ol_test.cno FROM student,ol_test WHERE ol_test.sno = student.sno");
    while(query1.next())
    {
        QString sclass1=query1.value(0).toString();
        QString test_no=query1.value(1).toString();
        if(student_class==sclass1&&course_no==test_no)
        {
            QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("该考试已发起过！"));
            box.setStandardButtons (QMessageBox::Ok);
            box.setButtonText (QMessageBox::Ok,tr("确 定"));
            box.exec ();
            return ;
        }
    }
    //将需要考试的班级记录在ol_test数据库中
    bool value=0;
    QString sclass;
    QSqlQuery query;
    query.exec("SELECT student.sno,student.sclass,teacher.cno FROM student , teacher\
               WHERE student.cno1=teacher.cno or student.cno2=teacher.cno");
    while(query.next())
    {
            QString student_sno=query.value(0).toString();
            sclass=query.value(1).toString();
            QString teacher_cno=query.value(2).toString();
            if(sclass==student_class&&teacher_cno==course_no)
            {
                QSqlQuery query1;

                value=query1.prepare("insert into ol_test(cno,tno,sno) values(:course,:teacher_no,:student_no)");
                if(value)
                {
                    query1.bindValue(":course",course_no);
                    query1.bindValue(":teacher_no",tno);
                    query1.bindValue(":student_no",student_sno);
                    query1.exec();


                }
            }

    }

         if(value)
      {
        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("成功发起考试,考试的班级是：")+ui->comboBox->currentText());
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
      }

}

   void Teacher::test_cancel()//撤销考试
{
    QString student_class=ui->comboBox->currentText();
    int flag=0;
    bool value=0;
    QSqlQuery query;
    query.exec("SELECT ol_test.cno,student.sclass,ol_test.sno FROM ol_test,student WHERE ol_test.sno = student.sno");
    while(query.next())
    {
        QString ol_test_cno=query.value(0).toString();
        QString student__class=query.value(1).toString();
        QString student_sno=query.value(2).toString();
        if(ol_test_cno==course_no&&student__class==student_class)
        {

            QSqlQuery query1;
            value=query1.prepare("delete from ol_test where cno=:str_sno and sno=:str_cno");
            if(value)
            {
                flag=1;
                query1.bindValue(":str_sno",ol_test_cno);
                query1.bindValue(":str_cno",student_sno);
                query1.exec();
            }
        }


    }
    if(flag==0)
    {
        QMessageBox box(QMessageBox::Warning,tr("系统消息"),tr("该班级关于")+ui->label_35->text()+tr("科目还没发起过考试！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
        return ;
    }
    if(value)
    {
        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("撤销考试成功,撤销考试的班级是：")+student_class);
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
    }

}

void Teacher::teacher_inform()//教师发布公告
{
   ui->stackedWidget->setCurrentIndex(5);
   connect(ui->pushButton_12,SIGNAL(clicked()),this,SLOT(submmit_inform()));
   connect(ui->pushButton_13,SIGNAL(clicked()),this,SLOT(clear_inform()));

}
   void Teacher::submmit_inform()//提交公告
   {
       QString str=ui->textEdit->toPlainText();
       bool value;
       QSqlQuery query;
       value=query.prepare("insert into t_inform(tno,neirong) values(:sno,:txt)");
       if(value)
       {
           query.bindValue(":sno",tno);
           query.bindValue(":txt",str);
           query.exec();

           QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("公告已发送！"));
           box.setStandardButtons (QMessageBox::Ok);
           box.setButtonText (QMessageBox::Ok,tr("确 定"));
           box.exec ();
           ui->textEdit->clear();
           ui->stackedWidget->setCurrentIndex(0);
           return ;
       }
       else
       {
           QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("发送失败"));
           box.setStandardButtons (QMessageBox::Ok);
           box.setButtonText (QMessageBox::Ok,tr("确 定"));
           box.exec ();
           return ;
       }

   }

   void Teacher::clear_inform()//清除公告内容
   {
       ui->textEdit->clear();
   }

void Teacher::teacher_talk_online()//留言墙
{
    ui->stackedWidget->setCurrentIndex(6);
    //设置聊天记录
    model->setTable("t_free_talk");
    model->setSort(0,Qt::DescendingOrder);
    model->setRelation(1,QSqlRelation("teacher","tno","tname"));
    model->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("内容:"));
    model->select();
    ui->tableView_4->setModel(model);
    ui->tableView_4->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置单元格为只读
    ui->tableView_4->verticalHeader()->hide();//隐藏tableview的导航
    //ui->tableView_4->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);//tableview列宽适应text
    ui->tableView_4->setColumnWidth(0,40);
    ui->tableView_4->setColumnWidth(2,285);

    connect(ui->pushButton_14,SIGNAL(clicked()),this,SLOT(free_talk_submmit()));
    connect(ui->pushButton_15,SIGNAL(clicked()),this,SLOT(clear_talk()));
}
    void Teacher::free_talk_submmit()//提交聊天记录
    {
        QSqlQuery query;
        query.exec("select tno,tspeak from teacher");
        while(query.next())
        {
            QString str1=query.value(0).toString();
            QString str2=query.value(1).toString();
            if(str1==tno)
            {
                if(str2=="0")
                {
                    QString str=ui->textEdit_2->toPlainText();
                    bool value;
                    QSqlQuery query;
                    value=query.prepare("insert into t_free_talk(tno,text)values(:sno,:txt)");
                    if(value)
                    {
                        query.bindValue(":sno",tno);
                        query.bindValue(":txt",str);
                        query.exec();

                        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("发送成功！"));
                        box.setStandardButtons (QMessageBox::Ok);
                        box.setButtonText (QMessageBox::Ok,tr("确 定"));
                        box.exec ();

                        ui->textEdit_2->clear();
                        model->setTable("t_free_talk");
                        model->setSort(0,Qt::DescendingOrder);
                        model->setRelation(1,QSqlRelation("teacher","tno","tname"));
                        model->setHeaderData(0,Qt::Horizontal,tr("序号"));
                        model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
                        model->setHeaderData(2,Qt::Horizontal,tr("内容"));
                        model->select();

                        ui->tableView_4->setModel(model);
                        //ui->tableView_4->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);//tableview列宽适应text
                        return ;

                     }
            }

                else
                    {

                    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("你已被禁言"));
                    box.setStandardButtons (QMessageBox::Ok);
                    box.setButtonText (QMessageBox::Ok,tr("确 定"));
                    box.exec ();

                    return ;
                }
        }

        }
    }
    void Teacher::clear_talk()//清除聊天输入框
    {
        ui->textEdit_2->clear();
    }

void Teacher::teacher_look_for_help()//老师寻求管理员帮助
{
     help=new teacher_need_help;
     help->show();
}

void Teacher::teacher_close()//撤销按钮安全退出
{
    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("安全退出！"));
    box.setStandardButtons (QMessageBox::Ok);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.exec ();
    this->close();

}

Teacher::~Teacher()
{
    delete ui;
}

//教师请求管理员帮助模块
teacher_need_help::teacher_need_help(QWidget *parent)
    :QDialog(parent)
{
    label=new QLabel;
    label->setText(tr("请您输入少于128个字的请求："));

    textedit=new QTextEdit;
    QVBoxLayout *top_layout=new QVBoxLayout;
    top_layout->addWidget(label);
    top_layout->addWidget(textedit);

    button1=new QPushButton;
    button1->setText(tr("提交"));
    button2=new QPushButton;
    button2->setText(tr("清空"));
    QHBoxLayout *layout_button=new QHBoxLayout;
    layout_button->addStretch();
    layout_button->addWidget(button1);
    layout_button->addWidget(button2);
    layout_button->addStretch();

    QVBoxLayout *mainlayout=new QVBoxLayout;
    mainlayout->addLayout(top_layout);
    mainlayout->addLayout(layout_button);

    setLayout(mainlayout);
    setWindowTitle(tr("请求帮助"));


    connect(button1,SIGNAL(clicked()),this,SLOT(teacher_summitall()));
    connect(button2,SIGNAL(clicked()),this,SLOT(teacher_clear_all()));
}

     void teacher_need_help::teacher_summitall()
{
         QString str=textedit->toPlainText();
         bool value;
         QSqlQuery query;
         value=query.prepare("insert into t_request(tno,text) values(:sno,:txt)");
         if(value)
         {
             query.bindValue(":sno",tno);
             query.bindValue(":txt",str);
             query.exec();

             QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("请求已发送至管理员！"));
             box.setStandardButtons (QMessageBox::Ok);
             box.setButtonText (QMessageBox::Ok,tr("确 定"));
             box.exec ();
             this->close();

             return ;

         }

}

     void teacher_need_help::teacher_clear_all()
{
        textedit->clear();
}

teacher_need_help::~teacher_need_help()
{

}
