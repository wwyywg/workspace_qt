#-------------------------------------------------
#
# Project created by QtCreator 2016-03-08T07:13:31
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = java_login
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    student.cpp \
    teacher.cpp \
    administractor.cpp

HEADERS  += dialog.h \
    student.h \
    teacher.h \
    administractor.h

FORMS    += dialog.ui \
    student.ui \
    teacher.ui \
    administractor.ui

RESOURCES += \
    java_login_resource.qrc
