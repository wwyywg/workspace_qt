#include "administractor.h"
#include "ui_administractor.h"
#include<QSqlRecord>
#include<QMessageBox>
#include<QSqlQuery>

Administractor::Administractor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Administractor)
{
    ui->setupUi(this);
    setWindowTitle(tr("后台客户端"));//设置窗口标题
    this->setFixedSize(this->width(),this->height());//固定窗口大小
    ui->stackedWidget->setCurrentIndex(0);

    //设置时间
    admini_timer=new QTimer(this);
    admini_timer->start(1000);
    connect(admini_timer,SIGNAL(timeout()),this,SLOT(admini_update_time()));

    model=new QSqlRelationalTableModel;
    model->setTable("student");
    model->setSort(0,Qt::AscendingOrder);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(0,Qt::Horizontal,tr("学号"));
    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("性别"));
    model->setHeaderData(3,Qt::Horizontal,tr("密码"));
    model->setHeaderData(4,Qt::Horizontal,tr("学院"));
    model->setHeaderData(5,Qt::Horizontal,tr("班级"));
    model->setHeaderData(6,Qt::Horizontal,tr("联系方式"));
    model->setHeaderData(7,Qt::Horizontal,tr("课程1"));
    model->setHeaderData(8,Qt::Horizontal,tr("课程2"));
    model->setHeaderData(9,Qt::Horizontal,tr("禁言"));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(student_manage()));
    connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(teacher_manage()));
    connect(ui->pushButton_15,SIGNAL(clicked()),this,SLOT(course_manage()));
    connect(ui->pushButton_4,SIGNAL(clicked()),this,SLOT(ad_say()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(question_table()));

    connect(ui->pushButton_8,SIGNAL(clicked()),this,SLOT(select_record()));
    connect(ui->pushButton_10,SIGNAL(clicked()),this,SLOT(update_record()));
    connect(ui->pushButton_9,SIGNAL(clicked()),this,SLOT(delete_record()));
    connect(ui->pushButton_7,SIGNAL(clicked()),this,SLOT(insert_record()));
    connect(ui->pushButton_12,SIGNAL(clicked()),this,SLOT(submmit_record()));
    connect(ui->pushButton_6,SIGNAL(clicked()),this,SLOT(exit()));

}
//时间变化函数
void Administractor::admini_update_time()
{
    QDateTime time=QDateTime::currentDateTime();
    QString str=time.toString("yyyy-MM-dd hh:mm:ss dddd");
    ui->label_6->setText(str);
}
//学生表
void Administractor::student_manage()
{
    ui->stackedWidget->setCurrentIndex(0);
    model->setTable("student");
    model->setSort(0,Qt::AscendingOrder);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(0,Qt::Horizontal,tr("学号"));
    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("性别"));
    model->setHeaderData(3,Qt::Horizontal,tr("密码"));
    model->setHeaderData(4,Qt::Horizontal,tr("学院"));
    model->setHeaderData(5,Qt::Horizontal,tr("班级"));
    model->setHeaderData(6,Qt::Horizontal,tr("联系方式"));
    model->setHeaderData(7,Qt::Horizontal,tr("课程1"));
    model->setHeaderData(8,Qt::Horizontal,tr("课程2"));
    model->setHeaderData(9,Qt::Horizontal,tr("禁言"));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);//tableview列宽适应text
    //ui->tableView->verticalHeader()->hide();
}
//教师表
void Administractor::teacher_manage()
{
    ui->stackedWidget->setCurrentIndex(0);
    model->setTable("teacher");
    model->setSort(0,Qt::AscendingOrder);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(0,Qt::Horizontal,tr("教工号"));
    model->setHeaderData(1,Qt::Horizontal,tr("姓名"));
    model->setHeaderData(2,Qt::Horizontal,tr("性别"));
    model->setHeaderData(3,Qt::Horizontal,tr("密码"));
    model->setHeaderData(4,Qt::Horizontal,tr("联系方式"));
    model->setHeaderData(5,Qt::Horizontal,tr("主讲课程"));
    model->setHeaderData(6,Qt::Horizontal,tr("禁言"));
    model->setHeaderData(7,Qt::Horizontal,tr("学院"));
    model->setHeaderData(8,Qt::Horizontal,tr("职位"));
    model->setHeaderData(9,Qt::Horizontal,tr("血型"));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);//tableview列宽适应text
    //ui->tableView_2->verticalHeader()->hide();
}
//课程表
void Administractor::course_manage()
{
    ui->stackedWidget->setCurrentIndex(0);
    model->setTable("course");
    model->setSort(0,Qt::AscendingOrder);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(0,Qt::Horizontal,tr("课程号"));
    model->setHeaderData(1,Qt::Horizontal,tr("课程名"));
    model->setHeaderData(2,Qt::Horizontal,tr("学分"));
    model->setHeaderData(3,Qt::Horizontal,tr("任课教师编号"));
    model->select();
    ui->tableView->setModel(model);


}
//公告
void Administractor::ad_say()
{
    ui->stackedWidget->setCurrentIndex(1);
    connect(ui->pushButton_13,SIGNAL(clicked()),this,SLOT(submmit_inform()));
    connect(ui->pushButton_14,SIGNAL(clicked()),this,SLOT(clear_text()));
    ui->pushButton_13->setEnabled(true);

}
void Administractor::submmit_inform()//提交公告
{
    QString str=ui->textEdit->toPlainText();
    bool value;
    QSqlQuery query;
    value=query.prepare("insert into admini_say(text)values(:txt)");
    if(value)
    {
        query.bindValue(":txt",str);
        query.exec();

        QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("公告已发送！"));
        box.setStandardButtons (QMessageBox::Ok);
        box.setButtonText (QMessageBox::Ok,tr("确 定"));
        box.exec ();
        ui->pushButton_13->setEnabled(false);
        ui->textEdit->clear();

    }
}
void Administractor::clear_text()//清除公告的输入框
{
    ui->textEdit->clear();
}

//疑问中心
void Administractor::question_table()
{
    ui->stackedWidget->setCurrentIndex(0);
    model->setTable("t_request");
    model->setSort(0,Qt::DescendingOrder);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(0,Qt::Horizontal,tr("序号"));
    model->setHeaderData(1,Qt::Horizontal,tr("教师号"));
    model->setHeaderData(2,Qt::Horizontal,tr("内容"));
    model->select();
    ui->tableView->setModel(model);

}

void Administractor::select_record()//查询操作
{
    QString filter = ui->lineEdit->text();
    if(ui->lineEdit->text().isEmpty())
    {
        return ;
    }

    QSqlRecord record=model->record();
    QString modelfilter;

    for(int i=0;i<record.count();++i)
    {

        if(i!=0)
        {
            modelfilter +=" or ";
        }
        QString fied=record.fieldName(i);
        QString subfilter=QString().sprintf("%s like '%%%s%%'", fied.toUtf8().data(),filter.toUtf8().data());

        modelfilter +=subfilter;
    }

    model->setFilter(modelfilter);
    model->select();
}
void Administractor::update_record()//修改操作
{
    model->database().transaction();
    if(model->submitAll())
    {
        model->database().commit();
        QMessageBox::information(NULL,tr("系统消息"),tr("修改成功"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
    }

    else
    {
        model->database().rollback();
        QMessageBox::critical(NULL,tr("系统消息"),tr("数据库错误"),QMessageBox::Yes);
    }
}
void Administractor::delete_record()//删除操作
{
    int curRow=ui->tableView->currentIndex().row();
    model->removeRow(curRow);
    int ok=QMessageBox::warning(NULL,tr("系统消息"),tr("确定删除当前行吗？"),QMessageBox::Yes|QMessageBox::No);
    if(ok==QMessageBox::No)
    {
        model->revertAll();
    }
    else
    {
        model->submitAll();
        QMessageBox::information(NULL,tr("系统消息"),tr("删除成功"),QMessageBox::Yes);
    }
}
void Administractor::insert_record()//添加操作
{
    int rownum=model->rowCount();

    model->insertRow(rownum);

    model->submitAll();
}
void Administractor::submmit_record()//提交操作
{
    QMessageBox::StandardButton ab=QMessageBox::warning(NULL,tr("系统消息"),tr("确认插入记录？"),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
    if(ab==QMessageBox::Yes)
    {
        model->submitAll();
        QMessageBox::information(NULL,tr("系统消息"),tr("添加记录成功"),QMessageBox::Yes);
    }
    else return ;
}
void Administractor::exit()//注销操作
{
    QMessageBox box(QMessageBox::Information,tr("系统消息"),tr("注销成功！"));
    box.setStandardButtons (QMessageBox::Ok);
    box.setButtonText (QMessageBox::Ok,tr("确 定"));
    box.exec ();
    this->close();
}


Administractor::~Administractor()
{
    delete ui;
}
