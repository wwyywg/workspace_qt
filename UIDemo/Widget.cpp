#include "Widget.h"

#include <QSqlDatabase>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("rwx850.cn");
    db.setPort(3306);
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("go_gateway");

    bool bRet = db.open();
    if(bRet == false) {
        qDebug()<< "Error open database";
        exit(0);
    }
    qDebug()<< "Open database successful";
}

Widget::~Widget()
{
}

